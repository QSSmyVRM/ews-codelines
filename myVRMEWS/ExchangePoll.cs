﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
#region using
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Collections.Specialized;
using Microsoft.Exchange.WebServices.Data;
using System.Net;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Security.Cryptography.X509Certificates;
using System.Linq;
using System.Xml;
using System.IO;
using System.Security.Cryptography;
using System.Globalization;
using System.Xml.Linq;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Office.Tools.Outlook;
using myVRMEWS.ExchangeReference;
using myVRMEWS.PushService;
#endregion
namespace ExcahngePollNms
{
    class ExcahngePoll
    {
        #region public properties
        string myVRMURL = "";
        string myVRMUsername = "";
        string myVRMPassword = "";
        string myVRMVersion = "";
        string myVRMClient = "";
        string filePath = "";
        string confType = "";
        string exchVersion = "";
        int completeDelete = 0;
        string UserSwitch = "";
        int FalseDelete = 0;
        int Deletecount = 0;
        string SSOMode = "";
        string ewsConfHostName = "EWS Admin";
        string ewsConfHostaddress = "admin@myvrm.com";
        int ErrorFetchCount = 0;//ZD 103314
        #endregion

        #region start

        public ExchangeService service = null;

        public void start()
        {
            System.Configuration.ConnectionStringSettings conSettings = null;
            string connectionString = "";
            NameValueCollection appSettings = null;
            string[] Values = null;
            DateTime strtDate = DateTime.UtcNow;
            DateTime endDate = DateTime.UtcNow;
            string errMsg = "";
            String[] delimiter = { ";" };
            int Pollcount = 0;
            String PassPhrase = "";
            List<RoomEWSDetails> RoomDetails = null;
            try
            {
                
                conSettings = ConfigurationManager.ConnectionStrings["MyDBConnectionString"];
                connectionString = conSettings.ConnectionString;
                appSettings = ConfigurationManager.AppSettings;
                Values = new string[appSettings.Count];
                for (int count = 0; count < appSettings.Count; count++)
                {
                    Values[count] = appSettings[count];
                }
                myVRMURL = Values[9];
                myVRMUsername = Values[10];
                myVRMPassword = Values[11];
                myVRMVersion = Values[12];
                myVRMClient = "07";
                filePath = Values[6];
                exchVersion = Values[7];
                Int32.TryParse(Values[14], out completeDelete);
                PassPhrase = Values[15];
                UserSwitch = Values[17];
                if (Values[16] == "1")
                {
                    Values[1] = Decrypt(Values[1], PassPhrase);
                    Values[0] = Decrypt(Values[0], PassPhrase);
                    Values[2] = Decrypt(Values[2], PassPhrase);
                    Values[3] = Decrypt(Values[3], PassPhrase);
                }

                if (!string.IsNullOrEmpty(appSettings["FalseDelete"]))
                    int.TryParse(appSettings["FalseDelete"], out FalseDelete);

                if (!string.IsNullOrEmpty(appSettings["SSOMode"]))
                    SSOMode = appSettings["SSOMode"];

                if (!string.IsNullOrEmpty(appSettings["confHostname"]))
                    ewsConfHostName = appSettings["confHostname"];

                if (!string.IsNullOrEmpty(appSettings["confHostAddress"]))
                    ewsConfHostaddress = appSettings["confHostAddress"];
                
                //ZD 103314
                if (!string.IsNullOrEmpty(appSettings["ErrorFetchCount"]))
                    int.TryParse(appSettings["ErrorFetchCount"], out ErrorFetchCount);

                WritetoLogfile("Polling Starts...");

                if (RetreiveRoomQueue(ref RoomDetails))
                {
                    if (int.TryParse(Values[13], out Pollcount))
                        endDate = strtDate.AddDays(Pollcount);
                    else
                        endDate = strtDate.AddDays(90);

                    strtDate = strtDate.Date;
                    endDate = endDate.Date.AddSeconds(86399);

                    strtDate = strtDate.Subtract(new TimeSpan(strtDate.Hour, strtDate.Minute, strtDate.Second));// to avoid deletions of confernece on the same day

                    GetExtendedPropertyData(RoomDetails, strtDate, endDate, "", Values[0], Values[1], Values[2], Values[3], Values[7], Values[8], connectionString);
                }

                WritetoLogfile("Polling Ends...");
            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                WritetoLogfile("On Start : " + errMsg);
            }

        }
        #endregion

        #region GetExtendedPropertyData (Getting the appointments from rooms)
        /// <summary>
        /// GetExtendedPropertyData
        /// </summary>
        /// <param name="RoomDetails"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="boardRoomID"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="domain"></param>
        /// <param name="url"></param>
        /// <param name="exchVersion"></param>
        /// <param name="httpsRequired"></param>
        /// <param name="connectionString"></param>
        public void GetExtendedPropertyData(List<RoomEWSDetails> RoomDetails, DateTime startDate, DateTime endDate, string boardRoomID, string username, string password, string domain, string url, string exchVersion, string httpsRequired, string connectionString) //ZD 101736
        {
            #region Region of variables

            string UID = "", Resources = "", errMsg = "", roomName = "", LyncMeetingURL = "", UCconfId = "", ConfLink = ""; //ALLDEV-862

            List<ExtendedAppointmentData> tmp = null;
            FindItemsResults<Appointment> appnts = null;
            List<Attendee> Attendees = null;
            List<string> finalLocation = null;
            
            Microsoft.Exchange.WebServices.Data.CalendarView view = null;
  
            myVRMEWS.ExchangeReference.ResolveNamesType type = null;
            myVRMEWS.ExchangeReference.ExchangeServiceBinding bind = null;
            myVRMEWS.ExchangeReference.ResolveNamesResponseType response = null;
            myVRMEWS.ExchangeReference.ResolveNamesResponseMessageType responseMessage = null;
            ExtendedAppointmentData addedItem = null;

            DateTime today = DateTime.Now.ToUniversalTime();
            string todayStr = today.ToString("dd MMMM yyyy 00:00");
            DateTime endPoll = endDate.ToUniversalTime();
            string endPollstr = endPoll.ToString("dd MMMM yyyy 23:59");
            Dictionary<string, DateTime> uniqueStorewithModified = new Dictionary<string, DateTime>();
            Dictionary<string, String> uniqueStorewithConfID = new Dictionary<string, String>();
            Dictionary<string, int> uniqueStorewithCancel = new Dictionary<string, int>();
            Dictionary<string, DateTime> uniqueStorewithEmpty = new Dictionary<string, DateTime>();
            Dictionary<string, int> uniqueStorewithErrorFetchCount = new Dictionary<string, int>();//ZD 103314
            DateTime modDate = DateTime.Now;
            List<String> attendEmail = null;

            List<ExtendedAppointmentData> finalAppoitnments = new List<ExtendedAppointmentData>();
            List<ExtendedAppointmentData> deletedAppoitnments = new List<ExtendedAppointmentData>();
            List<ExtendedAppointmentData> polledAppointments = new List<ExtendedAppointmentData>();
      
            ExtendedAppointmentData foundAppointment = null;
          
            #endregion
            try
            {
                #region Filling from conference create table

                SqlConnection myConnection = new SqlConnection(connectionString);
                SqlCommand mycommand = new SqlCommand();
                SqlDataAdapter dataAdapter = new SqlDataAdapter(mycommand);
                SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
                DataTable table = new DataTable();
                string[] location = null;
                string[] locationfromfield = null;

                #endregion

                WritetoLogfile("Filling up Exchange Details...");

                tmp = new List<ExtendedAppointmentData>();
                bind = new myVRMEWS.ExchangeReference.ExchangeServiceBinding();
                bind.Credentials = new NetworkCredential(username, password, domain);
                bind.Url = url;
                bind.Timeout = 300000;

                type = new myVRMEWS.ExchangeReference.ResolveNamesType();
                if (httpsRequired.Contains("Yes"))
                    System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender1, certificate, chain, sslPolicyErrors) => true);

                switch (exchVersion)
                {
                    case "2007":
                        service = new ExchangeService(ExchangeVersion.Exchange2007_SP1, TimeZoneInfo.Utc);
                        break;
                    case "2010":
                        service = new ExchangeService(ExchangeVersion.Exchange2010, TimeZoneInfo.Utc);
                        break;
                    case "2010_SP1":
                        service = new ExchangeService(ExchangeVersion.Exchange2010_SP1, TimeZoneInfo.Utc);
                        break;
                    case "2010_SP2":
                        service = new ExchangeService(ExchangeVersion.Exchange2010_SP2, TimeZoneInfo.Utc);
                        break;
                    case "365":
                        service = new ExchangeService(ExchangeVersion.Exchange2013, TimeZoneInfo.Utc);
                        break;
                    case "2013":
                        service = new ExchangeService(ExchangeVersion.Exchange2013, TimeZoneInfo.Utc);
                        break;
                    default:
                        service = new ExchangeService(ExchangeVersion.Exchange2013, TimeZoneInfo.Utc); 
                        break;
                }

                if (service == null)
                {
                    WritetoLogfile("Exchange Version Conflict");
                    return;
                }

                WritetoLogfile("Loading Fetched appointments Starts...");

                myConnection = new SqlConnection(connectionString);
                mycommand = new SqlCommand();
                dataAdapter = new SqlDataAdapter(mycommand);
                commandBuilder = new SqlCommandBuilder(dataAdapter);
                table = new DataTable();
                mycommand.Connection = myConnection;
                mycommand.CommandText = "SELECT distinct *,convert(datetime,startdate) as dtTime from ConferenceCreate where convert(datetime,startdate) >= convert(datetime,'" + todayStr + "') and convert(datetime,startdate) <= convert(datetime,'" + endPollstr + "') order by dtTime asc";
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                dataAdapter.Fill(table);
                dataAdapter.Dispose();
                FillUniqueStore(ref table, ref uniqueStorewithModified, ref uniqueStorewithCancel, ref uniqueStorewithEmpty, ref uniqueStorewithConfID, ref uniqueStorewithErrorFetchCount);//ZD 103314

                WritetoLogfile("Loading Fetched appointments Ends...");

                service.Url = new Uri(url);
                service.Credentials = new NetworkCredential(username, password);
                service.Timeout = 300000;

                view = new Microsoft.Exchange.WebServices.Data.CalendarView(startDate, endDate);
                view.StartDate = startDate;
                view.EndDate = endDate;
                view.Traversal = ItemTraversal.Shallow;
                view.MaxItemsReturned = 100;// Works because max of 96 conference of 15 mins in one day
                view.PropertySet = new PropertySet(BasePropertySet.IdOnly);
                ExtendedPropertyDefinition Property = new ExtendedPropertyDefinition(DefaultExtendedPropertySet.Appointment, 0x8238, MapiPropertyType.String);
				//ALLDEV-862 Start
                ExtendedPropertyDefinition LyncProperty = new ExtendedPropertyDefinition(DefaultExtendedPropertySet.PublicStrings, "UCOpenedConferenceID", MapiPropertyType.String);
                ExtendedPropertyDefinition OnlineMeetingExternalLink = new ExtendedPropertyDefinition(DefaultExtendedPropertySet.PublicStrings, "OnlineMeetingExternalLink", MapiPropertyType.String);
                ExtendedPropertyDefinition OnlineMeetingConfLink = new ExtendedPropertyDefinition(DefaultExtendedPropertySet.PublicStrings, "OnlineMeetingConfLink", MapiPropertyType.String);
                //ALLDEV-862 End
                // ALLDEV-863 Starts
                //ExtendedPropertyDefinition htmlBody = new ExtendedPropertyDefinition(0x1013, MapiPropertyType.Binary); //PR_BODY_HTML=0x001F or 0x1013 ?)

                PropertySet ItemPropertySet = new PropertySet(BasePropertySet.IdOnly, AppointmentSchema.DisplayTo, AppointmentSchema.Body, AppointmentSchema.Resources, AppointmentSchema.Subject,
                                        AppointmentSchema.RequiredAttendees, AppointmentSchema.OptionalAttendees, AppointmentSchema.Location, AppointmentSchema.TimeZone,
                                        AppointmentSchema.StartTimeZone, AppointmentSchema.EndTimeZone, AppointmentSchema.Start, AppointmentSchema.End,
                                        AppointmentSchema.LegacyFreeBusyStatus, AppointmentSchema.ICalUid, AppointmentSchema.ICalRecurrenceId,
                                        AppointmentSchema.ICalDateTimeStamp, AppointmentSchema.IsCancelled, AppointmentSchema.Organizer, AppointmentSchema.LastModifiedTime,
                                        AppointmentSchema.Duration, AppointmentSchema.IsRecurring, AppointmentSchema.AppointmentState,
                                        AppointmentSchema.LastModifiedName, OnlineMeetingExternalLink, LyncProperty, OnlineMeetingConfLink, Property); // 
                ItemPropertySet.Add(Property);
				//ALLDEV-862 Start
                ItemPropertySet.Add(LyncProperty);
                ItemPropertySet.Add(OnlineMeetingExternalLink);
                ItemPropertySet.Add(OnlineMeetingConfLink);
                //ALLDEV-862 End
                ItemPropertySet.RequestedBodyType = Microsoft.Exchange.WebServices.Data.BodyType.Text;
                // ALLDEV-863 Ends

                WritetoLogfile("Total Rooms Count :" + RoomDetails.Count);

                #region Loading Appointment Details

                for (int i = 0; i < RoomDetails.Count; i++)
                {
                    roomName = RoomDetails[i].email.Trim();
                    try
                    {
                        if (roomName != "")
                        {
                            WritetoLogfile("Fetching Appointments for Room : " + roomName);

                            appnts = service.FindAppointments(new FolderId(WellKnownFolderName.Calendar, roomName), view);
                            if (appnts != null)
                            {
                                WritetoLogfile("Total Polled Appointments : " + appnts.Items.Count);

                                if (appnts.Items.Count > 0)
                                {

                                    service.LoadPropertiesForItems(from Item item in appnts select item, ItemPropertySet); // ALLDEV-863 

                                    #region Making Appointmenet Ready

                                    foreach (var item in appnts.Items)
                                    {
                                        try
                                        {
                                            foundAppointment = null;
                                            //ALLDEV-862 Start
                                            LyncMeetingURL = "";
                                            if (item.TryGetProperty(LyncProperty, out UCconfId))
                                                if (item.TryGetProperty(OnlineMeetingExternalLink, out LyncMeetingURL))
                                                    if (item.TryGetProperty(OnlineMeetingConfLink, out ConfLink))
                                                    { }//EN-10415 : ZD 105583
                                            //ALLDEV-862 End
                                            if (item.IsRecurring && item.ICalRecurrenceId != null)
                                                UID = item.ICalUid.Trim() + "_" + item.ICalRecurrenceId.Value.ToLongDateString().Trim();
                                            else
                                                UID = item.ICalUid.Trim();

                                            foundAppointment = polledAppointments.FirstOrDefault(s => s.BoardroomID == UID);

                                            if (foundAppointment != null)
                                            {
                                                //if (!polledAppointments.FirstOrDefault(s => s.BoardroomID == UID).resourceEmail.Contains(roomName.ToLower().Trim()))
                                                //    polledAppointments.FirstOrDefault(s => s.BoardroomID == UID).resourceEmail.Add(roomName.ToLower().Trim());

                                                WritetoLogfile("Polled Meeting UID|Name : " + UID + "|" + item.Subject);

                                                continue;
                                            }

                                            if (completeDelete <= 0)
                                            {
                                                foundAppointment = deletedAppoitnments.FirstOrDefault(s => s.BoardroomID == UID);
                                                if (foundAppointment != null)
                                                {
                                                    if (item.IsCancelled)
                                                        continue;
                                                    else
                                                        deletedAppoitnments.Remove(foundAppointment);
                                                }
                                            }

                                            addedItem = new ExtendedAppointmentData();

                                            addedItem.EndDate = item.End.ToUniversalTime();
                                            addedItem.StartDate = item.Start.ToUniversalTime();

                                            WritetoLogfile("Into Resolving Room & User Information Start...");

                                            #region Resolving Rooms & Users
                                           
                                            // Resolving using Location Items
                                            location = null;
                                            finalLocation = new List<string>();
                                            locationfromfield = null;
                                            try
                                            {
                                                if (!string.IsNullOrEmpty(item.Location))
                                                {
                                                    WritetoLogfile("Polled Meeting Name|Locations : " + item.Subject + "|" + item.Location);
                                                    locationfromfield = item.Location.Split(';');
                                                }

                                                if (locationfromfield != null && locationfromfield.Length > 0)
                                                    location = locationfromfield;

                                                foreach (var loc in location)
                                                {
                                                    try
                                                    {
                                                        type.UnresolvedEntry = loc.Trim();
                                                        response = bind.ResolveNames(type);
                                                        if (response != null)
                                                        {
                                                            responseMessage = response.ResponseMessages.Items[0] as myVRMEWS.ExchangeReference.ResolveNamesResponseMessageType;
                                                            if (responseMessage.ResolutionSet != null && RoomDetails.Select(d => d.email.ToLower()).ToArray().Contains(responseMessage.ResolutionSet.Resolution[0].Mailbox.EmailAddress.Trim().ToLower()))
                                                            {
                                                                WritetoLogfile("Added Meeting Name|Locations : " + item.Subject + "|" + responseMessage.ResolutionSet.Resolution[0].Mailbox.EmailAddress.ToLower().Trim());
                                                                finalLocation.Add(responseMessage.ResolutionSet.Resolution[0].Mailbox.EmailAddress.ToLower().Trim());
                                                            }
                                                        }
                                                    }
                                                    catch (System.Exception ex10)
                                                    {
                                                        errMsg = "While resolving all attendees Message: " + ex10.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                                                        WritetoLogfile("Error Resolving using Location Items : " + errMsg);
                                                    }
                                                }
                                            }
                                            catch (System.Exception ex10)
                                            {
                                                errMsg = "While resolving all attendees Message: " + ex10.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                                                WritetoLogfile("Error Obtaining using Location Items : " + errMsg);
                                            }

                                            //Resolving Using Required Attendees Item
                                            Attendees = new List<Attendee>();
                                            attendEmail = new List<string>();
                                            try
                                            {
                                                if (item.RequiredAttendees != null)
                                                {
                                                    foreach (Attendee Attendee in item.RequiredAttendees)
                                                    {
                                                        attendEmail.Add(Attendee.Address);
                                                        //Commented & added for Pacific source phantom room check
                                                        //WritetoLogfile("Polled Meeting Name|Attendees : " + item.Subject + "|" + Attendee.Address.ToLower().Trim());
                                                        //if (!finalLocation.Contains(Attendee.Address.ToLower().Trim()))
                                                        //{
                                                        //    if (RoomDetails.Find(rmd => rmd.email.ToLower().Trim() == Attendee.Address.ToLower().Trim()) != null)
                                                        //    {
                                                        //        WritetoLogfile("Polled Meeting Name|LocationAttendees : " + item.Subject + "|" + Attendee.Address.ToLower().Trim());
                                                        //        finalLocation.Add(Attendee.Address.ToLower().Trim());
                                                        //    }
                                                        //    else
                                                        //        Attendees.Add(Attendee);
                                                        //}
                                                        Attendees.Add(Attendee);
                                                        // Pacific trace ends here
                                                    }
                                                }

                                                //Resolving Using Optional Attendees Item
                                                if (item.OptionalAttendees != null)
                                                {
                                                    foreach (Attendee Attendee in item.OptionalAttendees)
                                                    {
                                                        attendEmail.Add(Attendee.Address);
                                                        //Commented & added for Pacific source phantom room check
                                                        //WritetoLogfile("Polled Meeting Name|OptionalAttendees : " + item.Subject + "|" + Attendee.Address.ToLower().Trim());
                                                        //if (!finalLocation.Contains(Attendee.Address.ToLower().Trim()))
                                                        //{

                                                        //    if (RoomDetails.Find(rmd => rmd.email.ToLower().Trim() == Attendee.Address.ToLower().Trim()) != null)
                                                        //    {
                                                        //        WritetoLogfile("Polled Meeting Name|OptionalLocationAttendees : " + item.Subject + "|" + Attendee.Address.ToLower().Trim());
                                                        //        finalLocation.Add(Attendee.Address.ToLower().Trim());
                                                        //    }
                                                        //    else
                                                        //        Attendees.Add(Attendee);
                                                        //}
                                                        Attendees.Add(Attendee);
                                                        // Pacific trace ends here
                                                    }
                                                }
                                            }
                                            catch (System.Exception ex10)
                                            {
                                                errMsg = "While resolving all attendees Message: " + ex10.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                                                WritetoLogfile("Error Obtaining using Attendee Items : " + errMsg);
                                            }

                                            #endregion

                                            WritetoLogfile("Into Resolving Room & User Information End...");

                                            //ZD 103314 starts
                                            try
                                            {
                                                var addr = new System.Net.Mail.MailAddress(item.Organizer.Address);
                                                if (addr.Address != item.Organizer.Address)
                                                    item.Organizer.Address = ewsConfHostaddress;
                                            }
                                            catch (System.Exception e)
                                            {
                                                WritetoLogfile("Error in Host Email Address: " + e.Message);
                                                item.Organizer.Address = ewsConfHostaddress;
                                            }
                                            //ZD 103314 End
                                            addedItem.OrganizerID = item.Organizer.Address; //Need to Add unique ID along with it.
                                            addedItem.ItemIds = "";
                                            addedItem.IsModified = false;
                                            addedItem.IsCreated = false;
                                            addedItem.IsCancelCreated = false;
                                            addedItem.AppointmentStatus = item.LegacyFreeBusyStatus.ToString();

                                            if (item.Location != null)
                                                addedItem.Location = item.Location.ToString();
                                            else
                                                addedItem.Location = "";

                                            addedItem.BoardroomID = UID;
                                            addedItem.Subject = item.Subject;
                                            addedItem.LastModified = item.LastModifiedTime;
                                            addedItem.changeKey = item.Id.ChangeKey;
                                            addedItem.ownerName = item.Organizer.Name;
                                            addedItem.emailID = item.Organizer.Address;
                                            addedItem.LyncMeetingURL = ConfLink; //ALLDEV-862

                                            //If Room Email address as Organizer
                                            //if (RoomDetails != null)
                                            //{
                                            //    if (RoomDetails.Find(rmd => rmd.email.ToLower().Trim() == item.Organizer.Address.ToLower().Trim()) != null)
                                            //    {
                                            //        addedItem.ownerName = ewsConfHostName;
                                            //        addedItem.emailID = ewsConfHostaddress;
                                            //        finalLocation.Add(item.Organizer.Address.ToLower().Trim());
                                            //        WritetoLogfile("Polled Meeting Name|OrganizerLocation : " + item.Subject + "|" + item.Organizer.Address.ToLower().Trim());
                                            //    }
                                            //}

                                            //if (!finalLocation.Contains(roomName.ToLower().Trim()))
                                            //{
                                            //    WritetoLogfile("Polled Meeting Name|PolledLocation : " + item.Subject + "|" + roomName.ToLower().Trim());
                                            //    finalLocation.Add(roomName.ToLower().Trim());
                                            //}
                                            //finalLocation = finalLocation.Distinct().ToList();

                                            addedItem.LastModified = item.LastModifiedTime;
                                            addedItem.resourceEmail = finalLocation;
                                            addedItem.Duration = item.Duration.Minutes.ToString();

                                            if (Attendees != null)
                                                addedItem.AttendeeCollection = Attendees;
                                            else
                                                addedItem.AttendeeCollection = new List<Attendee>();

                                            addedItem.timeZone = item.TimeZone;
                                            addedItem.standardTimeZone = item.StartTimeZone.Id;

                                            if (item.Body != null)
                                                addedItem.Description = item.Body.Text;
                                            else
                                                addedItem.Description = "";

                                            confType = "2";
                                            if (finalLocation.Count < 2)
                                                confType = "7";

                                            addedItem.confType = confType;
                                            addedItem.appointmentState = item.AppointmentState;
                                            addedItem.ResourceID = Resources;
                                            addedItem.IsCancelled = item.IsCancelled;
                                            addedItem.confID = "";
                                            addedItem.IsRecurring = Convert.ToInt16(item.IsRecurring);

                                            modDate = DateTime.Now;
                                            modDate = item.LastModifiedTime.Subtract(new TimeSpan(0, 0, item.LastModifiedTime.Second));
											//ZD 103314
                                            if (uniqueStorewithModified.ContainsKey(UID) && uniqueStorewithModified[UID].Ticks < modDate.Ticks 
                                                || (uniqueStorewithErrorFetchCount.ContainsKey(UID) && 
                                                uniqueStorewithErrorFetchCount[UID] > 0 && uniqueStorewithErrorFetchCount[UID] < ErrorFetchCount))
                                            {
                                                if (addedItem.IsCancelled)
                                                {
                                                    addedItem.IsModified = false;
                                                    addedItem.IsCreated = false;
                                                    addedItem.IsCancelCreated = false;
                                                }
                                                else
                                                {
                                                    addedItem.IsModified = true;
                                                    addedItem.IsCreated = false;
                                                }
                                                addedItem.confID = uniqueStorewithConfID[UID].Trim();
                                            }
                                            else if (!uniqueStorewithModified.ContainsKey(UID))
                                            {
                                                addedItem.IsModified = false;
                                                addedItem.IsCreated = true;
                                            }

                                            if (uniqueStorewithModified.ContainsKey(UID) && uniqueStorewithModified[UID].Ticks == modDate.Ticks)
                                            {
                                                if (uniqueStorewithCancel[UID] == 1)
                                                {
                                                    if (!item.IsCancelled)
                                                    {
                                                        addedItem.IsModified = false;
                                                        addedItem.IsCreated = true;
                                                        addedItem.IsCancelCreated = true;
                                                    }
                                                    addedItem.confID = uniqueStorewithConfID[UID].Trim(); //ZD 104014
                                                }
                                            }

                                            if (addedItem.IsCancelled)
                                            {
                                                WritetoLogfile("Deleted Meeting Name : " + addedItem.Subject);
                                                deletedAppoitnments.Add(addedItem);
                                            }
                                            else
                                            {
                                                WritetoLogfile("Polled Meeting Name : " + addedItem.Subject);
												if(item.Body != null && item.Body.Text != null)
                                                	WritetoLogfile("Polled Meeting Description : " + item.Body.Text.ToString()); //ALLDEV-863
                                                
                                                polledAppointments.Add(addedItem);


                                                if (addedItem.StartDate.Subtract(DateTime.UtcNow).TotalMinutes <= 1) //For conferences spanning more than 2 days
                                                    continue;


                                                if (addedItem.IsCancelCreated || addedItem.IsCreated || addedItem.IsModified)
                                                    finalAppoitnments.Add(addedItem);
                                            }
                                        }
                                        catch (System.Exception e)
                                        {
                                            errMsg = "While creating appointments iten name :" + item.Subject + " Room Name:" + roomName + " Message: " + e.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                                            WritetoLogfile("Error in Filling Appointment Info : " + errMsg);
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }
                    }
                    catch (System.Exception e)
                    {
                        errMsg = "Room Name:" + roomName + " Message: " + e.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                        WritetoLogfile("Error in : " + errMsg);
                    }
                }
                #endregion

                WritetoLogfile("Into Pushing appointments to DB Start...");

                #region Pushing Appointment Details to myVRM
                try
                {
                    if (completeDelete == 1)
                        EmptyCalendarDelete(connectionString, ref table, ref polledAppointments);
                    else if (deletedAppoitnments.Count > 0)
                        CalendarDelete(connectionString, ref deletedAppoitnments, table);

                    foreach (ExtendedAppointmentData pollItems in finalAppoitnments)
                    {
                        InsertintoDB(pollItems, connectionString, ref table);
                    }
                }
                catch (System.Exception)
                { }
                #endregion

                WritetoLogfile("Into Pushing appointments to DB End...");
            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                WritetoLogfile("Error in GetExtendedPropertyData error :" + errMsg + " Stck trace : " + ex.StackTrace);
                WritetoLogfile("Error in GetExtendedPropertyData :" + RoomDetails.Count);
            }
        }
        #endregion

        #region InsertintoDB 
        /// <summary>
        /// Inserting the request and response from myvrm into DB
        /// </summary>
        /// <param name="item"></param>
        /// <param name="connectionString"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        private bool InsertintoDB(ExtendedAppointmentData item, string connectionString, ref DataTable table)
        {
            string errMsg = "", inXML = "", outXML = "", Subject = "", Location = "";
            string confId = "", instanceID = "", confnumname = "";
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand mycommand = new SqlCommand();
            mycommand.Connection = myConnection;
            DateTime Lastmodified = DateTime.MinValue;
            XmlDocument xml = null;
            DateTime today = DateTime.Now.ToUniversalTime();
            string todayStr = today.ToString("dd MMMM yyyy");
            try
            {
                Subject = ""; Location = "";
           
                if (item.Subject != null)
                {
                    if (item.Subject != "")
                        Subject = item.Subject.Replace("'", "");
                }
                else
                {
                    item.Subject = "**NO TITLE**";
                    Subject = "**NO TITLE**";
                }


                WritetoLogfile("Title : " + Subject);

                if (item.Location != null)
                {
                    if (item.Location != "")
                        Location = item.Location.Replace("'", "");
                }

                if (item.IsCreated || item.IsModified)
                {
                    WritetoLogfile("Insert or Update Appointment Title : " + Subject);

                    inXML = GenerateSetXML(item, item.confID);
                    outXML = WebrequestCall(inXML);
                    //ZD 103314 starts
                    string strErrorSmt = "0";
                    int isErrorConf = 0;
                    if (outXML == "")
                    {
                        isErrorConf = 1;
                        strErrorSmt = " ErrorFetchCount + 1 ";//ZD 103314
                        WritetoLogfile("Insert Appointment Error Subject : " + Subject + ". outXML is Empty.");
                    }//ZD 103314 end
                    else
                    {
                        xml = new XmlDocument();
                        xml.LoadXml(outXML);

                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            isErrorConf = 1;//ZD 103314
                            strErrorSmt = " ErrorFetchCount + 1 ";
                            outXML = outXML.Replace("'", "");
                            WritetoLogfile("Insert Appointment Error Subject : " + Subject + ". Error OUTXML :" + outXML);
                        }
                        else
                        {
                            isErrorConf = 0;//ZD 103314
                            strErrorSmt = "0";
                            if (xml.SelectSingleNode("/myVRM/command/setConference/conferences/conference/confID") != null)
                                confId = xml.SelectSingleNode("/myVRM/command/setConference/conferences/conference/confID").InnerText.Trim();

                            if (confId.Split(',').Length > 1)
                            {
                                instanceID = confId.Split(',')[1];
                                confId = confId.Split(',')[0];
                            }

                            if (xml.SelectSingleNode("/myVRM/command/setConference/conferences/conference/ConfUID") != null)
                                confnumname = xml.SelectSingleNode("/myVRM/command/setConference/conferences/conference/ConfUID").InnerText.Trim();
                        }
                    }
                    //ZD 103314 starts
                    try
                    {
                        Deletecount = 0;
                        myConnection.Open();
                        if (item.IsCancelCreated || item.IsModified)
                        {
                            if (confId != "" && confId != "0,0")
                                mycommand.CommandText = "UPDATE ConferenceCreate Set ErrorFetchCount = " + strErrorSmt + ", StartDate ='" + item.StartDate.ToString("dd MMMM yyyy hh:mm tt") + "',confStatus=" + item.appointmentState + ",inXML='" + inXML.Replace("'", "") + "',outXML='" + outXML.Replace("'", "") + "',EndDate='" + item.EndDate.ToString("dd MMMM yyyy hh:mm tt") + "',Subject='" + item.Subject.Replace("'", "") + "',Location='" + item.Location + "',isCancelled='0',changeKey='" + item.changeKey + "',LastModified = '" + item.LastModified.ToString("MM/dd/yyyy hh:mm tt") + "',confID='" + confId + "',LocationitemID='" + item.ItemIds + "',organizerItemID='" + item.OrganizerID + "',organizer='" + item.emailID + "',instanceID='" + instanceID + "',ConfNumName='" + confnumname + "',Deletecount=" + Deletecount + " Where ICALID ='" + item.BoardroomID.Trim() + "' and convert(datetime,startdate) >= convert(datetime,'" + todayStr + "')";
                            else
                                mycommand.CommandText = "UPDATE ConferenceCreate Set ErrorFetchCount = " + strErrorSmt + ", StartDate ='" + item.StartDate.ToString("dd MMMM yyyy hh:mm tt") + "',confStatus=" + item.appointmentState + ",inXML='" + inXML.Replace("'", "") + "',outXML='" + outXML.Replace("'", "") + "',EndDate='" + item.EndDate.ToString("dd MMMM yyyy hh:mm tt") + "',Subject='" + item.Subject.Replace("'", "") + "',Location='" + item.Location + "',isCancelled='0',changeKey='" + item.changeKey + "',LastModified = '" + item.LastModified.ToString("MM/dd/yyyy hh:mm tt") + "',LocationitemID='" + item.ItemIds + "',organizerItemID='" + item.OrganizerID + "',organizer='" + item.emailID + "',instanceID='" + instanceID + "',ConfNumName='" + confnumname + "',Deletecount=" + Deletecount + " Where ICALID ='" + item.BoardroomID.Trim() + "' and convert(datetime,startdate) >= convert(datetime,'" + todayStr + "')";
                        }
                        else
                        {
                            mycommand.CommandText = " IF EXISTS (select ICALID from ConferenceCreate where ICALID = '" + item.BoardroomID + "') "
                            + " UPDATE ConferenceCreate Set ErrorFetchCount = " + strErrorSmt + ", StartDate ='" + item.StartDate.ToString("dd MMMM yyyy hh:mm tt") + "',confStatus=" + item.appointmentState + ",inXML='" + inXML.Replace("'", "") + "',outXML='" + outXML.Replace("'", "") + "',EndDate='" + item.EndDate.ToString("dd MMMM yyyy hh:mm tt") + "',Subject='" + item.Subject.Replace("'", "") + "',Location='" + item.Location + "',isCancelled='0',changeKey='" + item.changeKey + "',LastModified = '" + item.LastModified.ToString("MM/dd/yyyy hh:mm tt") + "',confID='" + confId + "',LocationitemID='" + item.ItemIds + "',organizerItemID='" + item.OrganizerID + "',organizer='" + item.emailID + "',instanceID='" + instanceID + "',ConfNumName='" + confnumname + "',Deletecount=" + Deletecount + " Where ICALID ='" + item.BoardroomID.Trim() + "' and convert(datetime,startdate) >= convert(datetime,'" + todayStr + "')"
                            + " else INSERT INTO ConferenceCreate  ([ICALID],[StartDate],[confStatus],[EndDate],[IsRecurring],[organizer],[organizerItemID],[isCancelled] ,[inXML],[outXML],[Subject],[Location],[changeKey],[LastModified],[LocationitemID],[AttendeeitemID],[confID],[instanceID],[ConfNumName],[DeleteCount],[isExchangeConf],[ErrorFetchCount]) "
                                                         + "VALUES( '" + item.BoardroomID.Trim() + "','" + item.StartDate.ToString("dd MMMM yyyy hh:mm tt") + "','" + item.appointmentState + "','" + item.EndDate.ToString("dd MMMM yyyy hh:mm tt") + "'," + item.IsRecurring + ",'" + item.emailID + "','" + item.OrganizerID + "','0','" + inXML.Replace("'", "") + "','" + outXML.Replace("'", "") + "','" + Subject.Replace("'", "") + "','" + Location.Replace("'", "") + "','" + item.changeKey + "' ,'" + item.LastModified.ToString("MM/dd/yyyy hh:mm tt") + "','" + item.ItemIds + "','','" + confId + "','" + instanceID + "','" + confnumname + "'," + Deletecount + ",1," + isErrorConf + ")";
                        }
                        mycommand.ExecuteNonQuery();
                        myConnection.Close();
                    }
                    catch (System.Exception ex3)
                    {
                        errMsg = ex3.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                        WritetoLogfile("Error in updating DB : " + errMsg);
                    }

                    SaveAudit(myConnection, mycommand, item.BoardroomID.Trim(), item.StartDate.ToString("dd MMMM yyyy hh:mm"));
                }//ZD 103314 End
                return true;
            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                WritetoLogfile("Error in Inserting Appointments : " + errMsg);
                return false;
            }
        }
        #endregion

        #region EmptyCalendarDelete
        /// <summary>
        /// Delete Appointment if it wasn't Present on the calender after maximum polling
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="table"></param>
        /// <param name="Data"></param>
        /// <returns></returns>
        private bool EmptyCalendarDelete(string connectionString, ref DataTable table, ref List<ExtendedAppointmentData> Data)
        {
            string errMsg = "", inXML = "", outXML = "", confID = "";
            int isCancelled = 0;
            try
            {
                SqlConnection myConnection = new SqlConnection(connectionString);
                SqlCommand mycommand = new SqlCommand();
                mycommand.Connection = myConnection;
                DateTime Lastmodified = DateTime.MinValue;
                XmlDocument xml = null;
                DateTime today = DateTime.Now.ToUniversalTime();
                DateTime StartDate = DateTime.Now.ToUniversalTime();
                string todayStr = today.ToString("dd MMMM yyyy 00:00");
                ExtendedAppointmentData result = null;

                WritetoLogfile("Empty CalenderDelete Operation Start...");

                foreach (DataRow row in table.Rows)
                {
                    result = null; confID = ""; isCancelled = 0; Deletecount = 0; outXML = ""; inXML = "";//ZD 103314
                    if (Data.Count > 0)
                    {
                        result = Data.Find(
                           delegate(ExtendedAppointmentData exte)
                           {
                               return exte.BoardroomID == row["ICALID"].ToString().Trim();
                           }
                       );
                    }
                    if (!string.IsNullOrEmpty(row["confID"].ToString()))
                    {
                        if (row["confID"].ToString().Contains(','))
                        {
                            confID = row["confID"].ToString().Split(',')[0] + "," + row["instanceID"].ToString();
                        }
                        else
                            confID = row["confID"].ToString() + "," + row["instanceID"].ToString();
                    }

                    if (!string.IsNullOrEmpty(row["isCancelled"].ToString()))
                        int.TryParse(row["isCancelled"].ToString(), out isCancelled);

                    if (!string.IsNullOrEmpty(row["StartDate"].ToString()))
                        StartDate = Convert.ToDateTime(row["StartDate"].ToString());

                    if (!string.IsNullOrEmpty(row["DeleteCount"].ToString()))
                        int.TryParse(row["DeleteCount"].ToString(), out Deletecount);


                    if (isCancelled == 0 && StartDate.Subtract(today).TotalMinutes > 0 && result == null) //StartDate.Date >= today.Date 
                    {
                        if (Deletecount >= FalseDelete)
                        {
                            WritetoLogfile("conference for Delete after delete count of " + Deletecount.ToString() + " and date time diff of " + StartDate.Subtract(today).TotalMinutes.ToString() + "and Confid : " + confID);

                            inXML = GenerateCancelXML(confID);
                            outXML = WebrequestCall(inXML);
                            if (outXML != "")
                            {
                                xml = new XmlDocument();
                                xml.LoadXml(outXML);
                                if (outXML.IndexOf("<error>") >= 0)
                                {
                                    outXML = outXML.Replace("'", "");
                                }
                                else if (confID == "")
                                {
                                    if (xml.SelectSingleNode("/myVRM/command/delconference/conference/confID") != null)
                                        confID = xml.SelectSingleNode("/myVRM/command/delconference/conference/confID").InnerText;
                                }
                            }

                            isCancelled = 1;
                        }

                        Deletecount++;

                        myConnection.Open();
                        mycommand.CommandText = "UPDATE ConferenceCreate Set ICALID ='" + row["ICALID"].ToString().Trim() + "',StartDate ='" + row["StartDate"].ToString() + "',confStatus=" + row["confStatus"].ToString() + ",EndDate='" + row["EndDate"].ToString() + "',Subject='" + row["Subject"].ToString() + "',Location='" + row["Location"].ToString() + "'";
                        if (inXML != "" || outXML != "")
                            mycommand.CommandText += ", inXML='" + inXML.Replace("'", "") + "',outXML='" + outXML.Replace("'", "") + "'";

                        mycommand.CommandText += ", isCancelled='" + isCancelled + "' ,changeKey='" + row["changeKey"].ToString() + "',LastModified = '" + Convert.ToDateTime(row["LastModified"].ToString()).ToString("MM/dd/yyyy hh:mm tt") + "',DeleteCount=" + Deletecount + "  Where ICALID ='" + row["ICALID"] + "' and convert(datetime,startdate) >= convert(datetime,'" + todayStr + "')";
                        mycommand.ExecuteNonQuery();
                        myConnection.Close();

                        SaveAudit(myConnection, mycommand, row["ICALID"].ToString().Trim(), row["StartDate"].ToString().Trim());
                    }
                }

                WritetoLogfile("Empty CalenderDelete Operation End...");

                return true;
            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                WritetoLogfile("Error in Empty Calendar Delete : " + errMsg);
                return false;
            }
        }
        #endregion

        #region CalendarDelete
        /// <summary>
        /// Delete the appointment if meeting shows cancelled in calender
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="deletedItems"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        private bool CalendarDelete(string connectionString, ref List<ExtendedAppointmentData> deletedItems, DataTable table) 
        {
            string errMsg = "", inXML = "", outXML = "", filterExp = ""; 
            int isCancelled = 0;
            try
            {
                SqlConnection myConnection = new SqlConnection(connectionString);
                SqlCommand mycommand = new SqlCommand();
                mycommand.Connection = myConnection;
                XmlDocument xml = null;
                DateTime today = DateTime.Now.ToUniversalTime();
                string todayStr = today.ToString("dd MMMM yyyy");

                WritetoLogfile("Appointment Delete Operation Start...");

                foreach (ExtendedAppointmentData delItems in deletedItems)
                {
                    outXML = ""; filterExp = "";
                    if (delItems.IsCancelled && delItems.confID.Trim() != "")
                    {
                        filterExp = "UID='" + delItems.BoardroomID.Trim() + "'  AND isCancelled=0";
                        DataView dv = table.DefaultView;
                        dv.RowFilter = filterExp;

                        if (dv.ToTable().Columns["Deletecount"].ToString() != null)
                            int.TryParse(dv.ToTable().Columns["DeleteCount"].ToString(), out Deletecount);

                        if (Deletecount >= FalseDelete)
                        {
                            WritetoLogfile("Delete Appointment Title : " + delItems.Subject);
                            inXML = GenerateCancelXML(delItems.confID);
                            outXML = WebrequestCall(inXML);

                            if (outXML != "")
                            {
                                xml = new XmlDocument();
                                xml.LoadXml(outXML);
                                if (outXML.IndexOf("<error>") >= 0)
                                {
                                    outXML = outXML.Replace("'", "");
                                    WritetoLogfile("Error in Deleting Appointment Title : " + delItems.Subject + ". Error : " + outXML);
                                }
                            }
                            isCancelled = 1;
                        }
                        Deletecount++;

                        myConnection.Open();
                        mycommand.CommandText = "UPDATE ConferenceCreate Set UID ='" + delItems.BoardroomID.Trim() + "', isCancelled = '" + isCancelled + "', DeleteCount=" + Deletecount + "  Where UID ='" + delItems.BoardroomID.Trim() + "' and convert(datetime,startdate) >= convert(datetime,'" + todayStr + "')";
                        mycommand.ExecuteNonQuery();
                        myConnection.Close();
                    }
                }
                WritetoLogfile("Appointment Delete Operation Start...");
                return true;
            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                WritetoLogfile("Error in Calendar Delete : " + errMsg);
                return false;
            }
        }
        #endregion

        #region WebrequestCall
        /// <summary>
        /// WebrequestCall
        /// </summary>
        /// <param name="inXML"></param>
        /// <returns></returns>
        private string WebrequestCall(string inXML)
        {
            string outXML = "", errMsg = "";
            try
            {
                myVRMEWS.myVRMReference.VRMNewWebService service = new myVRMEWS.myVRMReference.VRMNewWebService();
                service.Url = myVRMURL;
                if (SSOMode.ToLower() == "true")
                    service.UseDefaultCredentials = true;
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender1, certificate, chain, sslPolicyErrors) => true);
                outXML = service.InvokeWebservice(inXML);

            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                WritetoLogfile("Error in Web request Call : " + errMsg);
            }
            return outXML;
        }
        #endregion

        #region GenerateXML
        /// <summary>
        /// Generate Conference Creation XML
        /// </summary>
        /// <param name="item"></param>
        /// <param name="confID"></param>
        /// <returns></returns>
        public string GenerateSetXML(ExtendedAppointmentData item, string confID)
        {
            string inXML = "";
            string Duration = "";
            string errMsg = "";
            try
            {
                Duration = item.EndDate.Subtract(item.StartDate).TotalMinutes.ToString();
                inXML = "<myVRM><login>" + myVRMUsername + "</login><password>" + myVRMPassword + "</password><version>" + myVRMVersion + "</version><client>7</client><commandname>SetConference</commandname><command><conference><userEMail>" + item.emailID + "</userEMail><confInfo>";
                if (confID != "" && confID != "0,0")
                    inXML += "<confID>" + confID + "</confID>";
                else
                    inXML += "<confID>new</confID>";
                item.Subject = item.Subject.Replace("&", "and").Replace("<", "").Replace(">", "").Replace("-", "");
                inXML += "<confName>" + item.Subject + "</confName>";

                WritetoLogfile("Conference Title is loaded");

                inXML += "<confHostEmail>" + item.emailID + "</confHostEmail>";
                inXML += "<confOrigin>7</confOrigin>";
                //ALLDEV-862 Start
                inXML += "<confLyncMeetingURL>" + item.LyncMeetingURL + "</confLyncMeetingURL>";
                inXML += "<isLyncConf>" + Convert.ToInt32(item.LyncMeetingURL.Length > 0 ? 1 : 0) + "</isLyncConf>";
                //ALLDEV-862 End
                inXML += "<startDate>" + item.StartDate.ToString("MM/dd/yyyy") + "</startDate>";
                inXML += "<startHour>" + item.StartDate.ToString("hh") + "</startHour>";
                inXML += "<startMin>" + item.StartDate.ToString("mm") + "</startMin>";
                inXML += "<startSet>" + item.StartDate.ToString("tt") + "</startSet>";
                inXML += "<standardTimeZone>" + item.standardTimeZone + "</standardTimeZone>";//Based on this tag, XMLRefactoring command will fetch the timezone.

                WritetoLogfile("Conference Timezone is loaded");

                inXML += "<timeZone>33</timeZone>";// Based on above tag, timezone will be fetched and replaced in this tag in XMLRefactoring method.
                inXML += "<setupDuration></setupDuration>";
                inXML += "<createBy>" + item.confType + "</createBy>";
                inXML += "<durationMin>" + Duration + "</durationMin>";
                //inXML += "<description>" + item.Subject + "</description>"; //addedItem.Description

                //ALLDEV-863 Starts
                string itemDes = "";
                string[] pipeDelim = { "Confidentiality Notice" };
                if (item.Description != null && item.Description != "")
                    itemDes = item.Description.Split(pipeDelim, StringSplitOptions.None)[0];

                inXML += "<description>" + itemDes.Replace("&", "σ").Replace("<", "õ").Replace(">", "æ") + "</description>";
                //ALLDEV-863 Ends
                
                inXML += "<IcalID>" + item.BoardroomID + "</IcalID>";

                if (item.resourceEmail.Count > 0)
                {
                    inXML += "<locationList>";
                    inXML += "<selected>";
                    foreach (var email in item.resourceEmail)
                    {
                        inXML += "<roomQueue>" + email + "</roomQueue>";
                    }
                    inXML += "</selected>";
                    inXML += "</locationList>";
                }

                WritetoLogfile("Conference Resource is loaded");

                if (item.AttendeeCollection.Count > 0)
                {
                    inXML += "<partys>";
                    foreach (Attendee Attendee in item.AttendeeCollection)
                    {
                        try
                        {
                            var addr = new System.Net.Mail.MailAddress(Attendee.Address);
                            Attendee.Address = addr.Address;
                        }
                        catch
                        {
                            WritetoLogfile("Invalid Party address :" + Attendee.Address);
                            continue;
                        }

                        inXML += "<party>";
                        inXML += "<partyID>new</partyID>";
                        inXML += "<partyFirstName>" + Attendee.Name.Replace("&", "") + "</partyFirstName>";
                        inXML += "<partyLastName>" + Attendee.Name.Replace("&", "") + "</partyLastName>";
                        inXML += "<partyEmail>" + Attendee.Address + "</partyEmail>";
                        inXML += "<partyInvite>2</partyInvite>";
                        inXML += "<partyNotify>1</partyNotify>";
                        inXML += "<partyAudVid>1</partyAudVid>";
                        inXML += "<notifyOnEdit>1</notifyOnEdit>";
                        inXML += "</party>";
                    }
                    inXML += "</partys>";
                }

                WritetoLogfile("Conference Party is loaded");

                inXML += "</confInfo>";
                inXML += "</conference>";
                inXML += "</command>";
                inXML += "</myVRM>";

            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                WritetoLogfile("Error in Generating Set XML : " + errMsg);
            }
            return inXML;
        }

        public string GenerateCancelXML(string confID)
        {
            string inXML = "";
            string errMsg = "";
            try
            {
                inXML = "<myVRM><login>" + myVRMUsername + "</login><password>" + myVRMPassword + "</password>";
                inXML += "<version>" + myVRMVersion + "</version><client>7</client>";
                inXML += "<commandname>DeleteConference</commandname>";
                inXML += "<command><login><userID>11</userID>";
                inXML += "<delconference><conference>";
                inXML += "<confID>" + confID + "</confID>";
                inXML += "<reason>deletedforAPItest</reason>";
                inXML += "</conference></delconference>";
                inXML += "</login></command></myVRM>";
            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                WritetoLogfile("Error in Generating Cancel XML : " + errMsg);
            }
            return inXML;
        }

        public string GenerateRoomUpdateXML(List<RoomEWSDetails> RoomDetails)
        {
            string inXML = "";
            string errMsg = "";
            try
            {
                inXML = "<myVRM><login>" + myVRMUsername + "</login><password>" + myVRMPassword + "</password>";
                inXML += "<version>" + myVRMVersion + "</version><client>7</client>";
                inXML += "<commandname>UpdateRoomEWSDetails</commandname>";
                inXML += "<command><login><userID>11</userID>";
                inXML += "<UpdateRoomEWSDetails>";
                for (int r = 0; r < RoomDetails.Count; r++)
                {
                    inXML += "<RoomDetails>";
                    inXML += "<RoomId>" + RoomDetails[r].RoomId + "</RoomId>";
                    inXML += "<RoomEWSURL>" + RoomDetails[r].EWSurl + "</RoomEWSURL>";
                    inXML += "</RoomDetails>";
                }
                inXML += "</UpdateRoomEWSDetails>";
                inXML += "</login></command></myVRM>";
            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                WritetoLogfile("Error in Generating RoomUpdate XML : " + errMsg);
            }
            return inXML;
        }
 
        #endregion

        #region Retreive room queue
        /// <summary>
        /// RetreiveRoomQueue
        /// </summary>
        /// <param name="RoomDetails"></param>
        /// <returns></returns>
        public bool RetreiveRoomQueue(ref List<RoomEWSDetails> RoomDetails)
        {
            string errMsg = "", inXML = "", outXML = "";
            XDocument xdoc = null;
            try
            {
                WritetoLogfile("Retreiving Room information Starts...");

                inXML = "<myVRM><login>" + myVRMUsername + "</login><password>" + myVRMPassword + "</password><version>" + myVRMVersion + "</version><client>07</client><commandname>GetRoomEWSDetails</commandname><command><GetRoomEWSDetails><userLogin>" + myVRMUsername + "</userLogin><organizationID>11</organizationID></GetRoomEWSDetails></command></myVRM>";
                outXML = WebrequestCall(inXML);
                WritetoLogfile(outXML);

                if (outXML != "")
                {
                    xdoc = XDocument.Parse(outXML);
                    RoomDetails = (from temp in xdoc.Descendants("RoomDetails").OrderBy(x => x.Element("RoomEWSURL").Value)
                                   select new RoomEWSDetails
                                   {
                                       RoomId = Convert.ToInt32(temp.Element("RoomId").Value),
                                       email = ((string.IsNullOrEmpty(temp.Element("RoomEmail").Value) == true) ? "" : Convert.ToString(temp.Element("RoomEmail").Value)),
                                       domain = ((string.IsNullOrEmpty(temp.Element("RoomDomain").Value) == true) ? "" : Convert.ToString(temp.Element("RoomDomain").Value)),
                                       EWSurl = ((string.IsNullOrEmpty(temp.Element("RoomEWSURL").Value) == true) ? "" : Convert.ToString(temp.Element("RoomEWSURL").Value)),
                                   }).ToList();
                }

                WritetoLogfile("Retreiving Room information Ends...");
            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                WritetoLogfile("Error in Retreive Room Queue : " + errMsg);
            }
            return true;
        }

        #endregion

        #region Write to log file
        /// <summary>
        /// WritetoLogfile
        /// </summary>
        /// <param name="error"></param>
        public void WritetoLogfile(String error)
        {
            StreamWriter sw = null;
            String logFilePath = filePath;
            String newLogfilepath = "";
            try
            {
                string sYear = DateTime.Now.Year.ToString();
                string sMonth = DateTime.Now.Month.ToString();
                string sDay = DateTime.Now.Day.ToString();

                string lgName = sYear + sMonth + sDay;

                newLogfilepath = logFilePath;

                newLogfilepath = newLogfilepath + "_" + lgName + ".log";

                if (!File.Exists(newLogfilepath))
                {
                    // file doesnot exist . hence, create a new log file.
                    sw = File.CreateText(newLogfilepath);
                    sw.Flush();
                    sw.Close();
                }
                else
                {
                    // check if exisiting log file size is greater than 50 MB
                    FileInfo fi = new FileInfo(newLogfilepath);
                    if (fi.Length > 100000000)
                    {
                        // delete the log file						
                        File.Delete(logFilePath);
                        // create a new log file 
                        sw = File.CreateText(newLogfilepath);
                        sw.Flush();
                        sw.Close();
                    }
                }
                // write the log record.
                sw = File.AppendText(newLogfilepath);
                sw.WriteLine(error);
                sw.Flush();
                sw.Close();
            }
            catch (System.Exception)
            {
                // do nothing
            }

        }

        #endregion

        #region Fill Unique Store
        /// <summary>
        /// FillUniqueStore
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="uniqueStorewithModified"></param>
        /// <param name="uniqueStorewithCancel"></param>
        /// <param name="uniqueStorewithEmpty"></param>
        /// <param name="uniqueStorewithConfID"></param>
        public void FillUniqueStore(ref DataTable dt, ref Dictionary<string, DateTime> uniqueStorewithModified, ref Dictionary<string, int> uniqueStorewithCancel, ref Dictionary<string, DateTime> uniqueStorewithEmpty, ref Dictionary<string, string> uniqueStorewithConfID, ref Dictionary<string, int> uniqueStorewithErrorFetchCount)//ZD 103314
        {
            string errMsg = "", confID = "";
            DateTime Lastmodified = DateTime.MinValue;
            int iscancelled = 0, errorFetchCount = 0;//ZD 103314
            string[] tempConfID = new string[2];
            try
            {
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int cnt = 0; cnt < dt.Rows.Count; cnt++)
                        {
                            if (uniqueStorewithModified == null)
                                uniqueStorewithModified = new Dictionary<string, DateTime>();

                            if (uniqueStorewithConfID == null)
                                uniqueStorewithConfID = new Dictionary<string, string>();

                            if (!uniqueStorewithModified.ContainsKey(dt.Rows[cnt]["ICALID"].ToString().Trim()))
                            {
                                try
                                {
                                    Lastmodified = DateTime.MinValue;
                                    if (!DateTime.TryParse(dt.Rows[cnt]["LastModified"].ToString(), out Lastmodified))
                                        Lastmodified = DateTime.ParseExact(dt.Rows[cnt]["LastModified"].ToString(), "dd/MM/yyyy HH:mm:ss", null);

                                    if (!Int32.TryParse(dt.Rows[cnt]["isCancelled"].ToString(), out iscancelled))
                                        iscancelled = 0;
									//ZD 103314
                                    int.TryParse(dt.Rows[cnt]["ErrorFetchCount"].ToString(), out errorFetchCount);

                                    confID = "";
                                    tempConfID = dt.Rows[cnt]["confID"].ToString().Split(',');
                                    confID = tempConfID[0] + "," + dt.Rows[cnt]["instanceID"].ToString();
                                    uniqueStorewithConfID.Add(dt.Rows[cnt]["ICALID"].ToString().Trim(), confID);
                                    uniqueStorewithModified.Add(dt.Rows[cnt]["ICALID"].ToString().Trim(), Lastmodified);
                                    uniqueStorewithCancel.Add(dt.Rows[cnt]["ICALID"].ToString().Trim(), iscancelled);
                                    uniqueStorewithErrorFetchCount.Add(dt.Rows[cnt]["ICALID"].ToString().Trim(), errorFetchCount);//ZD 103314

                                }
                                catch (System.Exception ex1)
                                {
                                    errMsg = ex1.Message + "Problem in date conversion while filling unique store " + dt.Rows[cnt]["LastModified"].ToString();
                                    WritetoLogfile("Error in Filling Details : " + errMsg);
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                WritetoLogfile("Error in Filling Unique Store : " + errMsg);
            }
        }

        #endregion

        #region Fill Audit table
        /// <summary>
        /// Save Audit table
        /// </summary>
        /// <param name="myConnection"></param>
        /// <param name="myCommand"></param>
        /// <param name="icalID"></param>
        /// <param name="startDate"></param>
        private void SaveAudit(SqlConnection myConnection, SqlCommand myCommand, string icalID, string startDate)
        {
            string errMsg = "";
            DateTime Lastmodified = DateTime.MinValue;
            try
            {
                if (myCommand != null && myConnection != null)
                {
                    myConnection.Open();
                    myCommand.CommandText = "INSERT INTO [ConferenceCreateAudit]"
          + "  ([confID]"
         + "   ,[instanceID]"
         + "   ,[ConfNumName] "
         + "   ,[Subject] "
         + "   ,[confStatus]"
         + "   ,[StartDate] "
         + "   ,[EndDate] "
         + "   ,[IsRecurring] "
         + "   ,[Location] "
         + "   ,[organizer] "
         + "   ,[organizerItemID] "
         + "   ,[isCancelled] "
         + "   ,[inXML] "
         + "   ,[outXML]"
         + "   ,[changeKey]"
         + "   ,[LastModified]"
         + "   ,[AttendeeitemID]"
         + "   ,[LocationitemID]"
         + "   ,[ICALID]"
         + " ,[AuditDate]"
         + ",[Deletecount]) "
         + "  SELECT [confID] "
         + " ,[instanceID] "
         + " ,[ConfNumName] "
         + " ,[Subject] "
         + " ,[confStatus] "
         + " ,[StartDate] "
         + " ,[EndDate] "
          + ",[IsRecurring] "
         + " ,[Location] "
         + " ,[organizer] "
         + " ,[organizerItemID] "
          + ",[isCancelled] "
         + " ,[inXML] "
         + " ,[outXML] "
          + ",[changeKey] "
         + " ,[LastModified] "
          + ",[AttendeeitemID]"
          + ",[LocationitemID] "
          + ",[ICALID],GETDATE()"
          + ",[Deletecount]"
          + "FROM [ConferenceCreate] where ICALID = '" + icalID + "' and  convert(datetime,startdate) = convert(datetime,'" + startDate + "')"; //ZD 101220
                    myCommand.ExecuteNonQuery();
                    myConnection.Close();
                }
            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                WritetoLogfile("Error in Saving Audit Information : " + errMsg);
            }
        }

        #endregion

        #region ExtendedAppointmentData Class
        public class ExtendedAppointmentData
        {
            public DateTime EndDate = DateTime.MinValue;
            public DateTime StartDate = DateTime.MinValue;
            public DateTime LastModified = DateTime.MinValue;
            public String EventID = "";
            public String AppointmentStatus = "";
            public String BoardroomID = "";
            public String Location = "";
            public String Subject = "";
            public String changeKey = "";
            public String ownerName = "";
            public String emailID = "";
            public String Duration = "";
            public String timeZone = "";
            public String standardTimeZone = "";
            public String Description = "";
            public String confType = "";
            public String ResourceID = "";
            public Appointment appt = null;
            public bool IsCancelled = false;
            public int appointmentState = 0;
            public List<String> resourceEmail = new List<String>();
            public List<Attendee> AttendeeCollection = new List<Attendee>();
            public bool IsCreated = false;
            public bool IsModified = false;
            public bool IsCancelCreated = false;
            public String LastModifiedName = "";
            public String ItemIds = "";
            public String OrganizerID = "";
            public String confID = "";
            public int IsRecurring = 0;
            public string LyncMeetingURL = ""; //ALLDEV-862
        }
        #endregion

        #region RoomEWSDetails
        public class RoomEWSDetails
        {
            internal int RoomId { get; set; }
            internal string email { get; set; }
            internal string domain { get; set; }
            internal string EWSurl { get; set; }
        }
        #endregion

        #region Decrypt(string , string )
        public static string Decrypt(string cipherText, string Password)
        {
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65,
            0x64, 0x76, 0x65, 0x64, 0x65, 0x76});
            byte[] decryptedData = Decrypt(cipherBytes,
               pdb.GetBytes(32), pdb.GetBytes(16));
            return System.Text.Encoding.Unicode.GetString(decryptedData);
        }
        #endregion

        #region Decrypt(byte[] ,byte[], byte[])
        public static byte[] Decrypt(byte[] cipherData,
                            byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();
            alg.Key = Key;
            alg.IV = IV;
            CryptoStream cs = new CryptoStream(ms,
                alg.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(cipherData, 0, cipherData.Length);
            cs.Close();
            byte[] decryptedData = ms.ToArray();
            return decryptedData;
        }
        #endregion

        // Below Methods not used

        #region Compare XML
        public bool Comparexml(String inXMLstr, String oldXmlstr)
        {
            XmlDocument newXml = null;
            XmlDocument oldXml = null;
            XmlNodeList oldRoom = null;
            XmlNodeList newRoom = null;
            XmlNodeList olduser = null;
            XmlNodeList newUser = null;
            String errMsg = "";
            Boolean isMatchFound = false;
            try
            {
                inXMLstr = inXMLstr.Replace("'", "");
                newXml = new XmlDocument();
                newXml.LoadXml(inXMLstr);
                oldXml = new XmlDocument();
                oldXml.LoadXml(oldXmlstr);

                if (oldXml.SelectSingleNode("//myVRM/command/conference/confInfo/confName").InnerText.Trim() != newXml.SelectSingleNode("//myVRM/command/conference/confInfo/confName").InnerText.Trim())
                    return false;
                if (oldXml.SelectSingleNode("//myVRM/command/conference/confInfo/confHostEmail").InnerText.Trim() != newXml.SelectSingleNode("//myVRM/command/conference/confInfo/confHostEmail").InnerText.Trim())
                    return false;
                if (oldXml.SelectSingleNode("//myVRM/command/conference/confInfo/startDate").InnerText.Trim() != newXml.SelectSingleNode("//myVRM/command/conference/confInfo/startDate").InnerText.Trim())
                    return false;
                if (oldXml.SelectSingleNode("//myVRM/command/conference/confInfo/startHour").InnerText.Trim() != newXml.SelectSingleNode("//myVRM/command/conference/confInfo/startHour").InnerText.Trim())
                    return false;
                if (oldXml.SelectSingleNode("//myVRM/command/conference/confInfo/startMin").InnerText.Trim() != newXml.SelectSingleNode("//myVRM/command/conference/confInfo/startMin").InnerText.Trim())
                    return false;
                if (oldXml.SelectSingleNode("//myVRM/command/conference/confInfo/startSet").InnerText.Trim() != newXml.SelectSingleNode("//myVRM/command/conference/confInfo/startSet").InnerText.Trim())
                    return false;
                if (oldXml.SelectSingleNode("//myVRM/command/conference/confInfo/durationMin").InnerText.Trim() != newXml.SelectSingleNode("//myVRM/command/conference/confInfo/durationMin").InnerText.Trim())
                    return false;

                olduser = oldXml.SelectNodes("//myVRM/command/conference/confInfo/partys/party");
                newUser = newXml.SelectNodes("//myVRM/command/conference/confInfo/partys/party");

                foreach (XmlNode item in newUser)
                {
                    isMatchFound = false;
                    foreach (XmlNode item2 in olduser)
                    {
                        if (item2.SelectSingleNode("//partyEmail").InnerText.Contains(item.SelectSingleNode("//partyEmail").InnerText))
                        {
                            isMatchFound = true;
                            if (isMatchFound)
                                break;
                        }
                    }

                    if (!isMatchFound)
                        return false;
                }

                oldRoom = oldXml.SelectNodes("//myVRM/command/conference/confInfo/locationList/selected/roomQueue");
                newRoom = newXml.SelectNodes("//myVRM/command/conference/confInfo/locationList/selected/roomQueue");
                if (oldRoom.Count != newRoom.Count)
                    return false;

                foreach (XmlNode item in newRoom)
                {
                    isMatchFound = false;
                    foreach (XmlNode item2 in oldRoom)
                    {
                        if (item2.InnerText.Contains(item.InnerText))
                        {
                            isMatchFound = true;
                            if (isMatchFound)
                                break;

                        }
                    }

                    if (!isMatchFound)
                        return false;
                }
            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                
            }

            return true;
        }
        #endregion

        #region Update last modified
        /// <summary>
        /// Update the conference last modified so it doenst keep poping up
        /// </summary>
        /// <param name="myConnection"></param>
        /// <param name="myCommand"></param>
        /// <param name="dr"></param>
        /// <param name="UID"></param>
        /// <param name="startDate"></param>
        public void UpdateLastModified(SqlConnection myConnection, SqlCommand myCommand, ExtendedAppointmentData item, String todayStr)
        {
            string errMsg = "";
            try
            {
                if (myCommand != null && myConnection != null)
                {
                    myConnection.Open();
                    myCommand.CommandText = "UPDATE ConferenceCreate Set LastModified = '" + item.LastModified.ToString("MM/dd/yyyy hh:mm tt") + "' Where UID ='" + item.BoardroomID.Trim() + "' and convert(datetime,startdate) >= convert(datetime,'" + todayStr + "')";
                    myCommand.ExecuteNonQuery();
                    myConnection.Close();

                }
            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
               
            }
        }

        #endregion

    }
}
