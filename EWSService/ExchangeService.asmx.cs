﻿//ZD 100147 Start
/* Copyright (C) 2013 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Collections.Specialized;
using Microsoft.Exchange.WebServices.Data;
using System.Net;
using System.Linq;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Security.Cryptography;
using System.Globalization;
using EWSService.ExchangeReference;
using EWSService.MyvrmReference;

namespace EWSService
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService
    {
        
        #region public properties

        string myVRMURL = "", myVRMUsername = "";
        string myVRMPassword = "", myVRMVersion = "";
        string filePath = "", exchVersion = "";
        string myVRMClient = "", exchUserName = "";
        string exchPassword = "", exchURL = "";
        string exchDomain = "", isHttpsRequired = "";
        string connectionString = "", myVRMUserAccess = "";
        static NS_LOGGER.Log log = null;
        ExtendedPropertyDefinition ExtPropertyDef = null;
        Microsoft.Exchange.WebServices.Data.CalendarView view = null;
        FindItemsResults<Appointment> appnts = null;
        ExchangeService service = null;
        #endregion

        #region Reading Credentials
        /// <summary>
        /// GetApplicationSettings
        /// </summary>
        private void GetApplicationSettings()
        {
            String PassPhrase = "";
            string[] Values = null;
            System.Configuration.ConnectionStringSettings conSettings = null;

            NameValueCollection appSettings = null;
            try
            {
                

                conSettings = ConfigurationManager.ConnectionStrings["MyDBConnectionString"];
                connectionString = conSettings.ConnectionString;
                appSettings = ConfigurationManager.AppSettings;
                Values = new string[appSettings.Count];
                for (int count = 0; count < appSettings.Count; count++)
                {
                    Values[count] = appSettings[count];
                }
                myVRMURL = Values[8];
                myVRMUsername = Values[9];
                myVRMPassword = Values[10];
                myVRMVersion = Values[11];
                myVRMClient = "07";
                filePath = Values[5];
                exchVersion = Values[6];
                PassPhrase = Values[14];
                exchURL = Values[3];
                exchUserName = Values[0];
                exchPassword = Values[1];
                exchDomain = Values[2];
                isHttpsRequired = Values[7];
                if (Values[15] == "1")
                {
                    exchPassword = Decrypt(Values[1], PassPhrase);
                    exchUserName = Decrypt(Values[0], PassPhrase);
                    exchDomain = Decrypt(Values[2], PassPhrase);
                    exchURL = Decrypt(Values[3], PassPhrase);
                }
                myVRMUserAccess = Values[16];
                log = new NS_LOGGER.Log(filePath);
                log.Trace("Application config read ends...");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Exchange Service Binding
        /// <summary>
        /// ExchangeServiceBinding
        /// </summary>
        private bool ExchangeServiceBinding()
        {
            EWSService.ExchangeReference.ExchangeServiceBinding bind = null;
            try
            {
                log.Trace("Binding User Credenitail starts...");

                bind = new EWSService.ExchangeReference.ExchangeServiceBinding();
                bind.Credentials = new NetworkCredential(exchUserName, exchPassword, exchDomain);
                bind.Url = exchURL;
                bind.Timeout = 300000;
                if (isHttpsRequired.Contains("Yes"))
                    System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender1, certificate, chain, sslPolicyErrors) => true);
                if (exchVersion.Contains("2010") || exchVersion.Contains("365")) //ZD 101872
                    service = new ExchangeService(ExchangeVersion.Exchange2010, TimeZoneInfo.Utc);
                if (service == null)
                {
                    return false;
                }
                else if (exchVersion.Contains("2007_SP1"))//Version Check
                {
                    service = new ExchangeService(ExchangeVersion.Exchange2010, TimeZoneInfo.Utc);
                    if (service != null)
                    {
                        return false;
                    }
                }
                service.Url = new Uri(exchURL);
                service.Credentials = new NetworkCredential(exchUserName, exchPassword);
                service.Timeout = 300000;

                log.Trace("Binding User Credenitail ends...");
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Decrypt(string , string )
        /// <summary>
        /// Decrypt
        /// </summary>
        /// <param name="cipherText"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public static string Decrypt(string cipherText, string Password)
        {
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(Password,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65,
            0x64, 0x76, 0x65, 0x64, 0x65, 0x76});
            byte[] decryptedData = Decrypt(cipherBytes,
               pdb.GetBytes(32), pdb.GetBytes(16));
            return System.Text.Encoding.Unicode.GetString(decryptedData);
        }
        #endregion

        #region Decrypt(byte[] ,byte[], byte[])
        /// <summary>
        /// Decrypt
        /// </summary>
        /// <param name="cipherData"></param>
        /// <param name="Key"></param>
        /// <param name="IV"></param>
        /// <returns></returns>
        public static byte[] Decrypt(byte[] cipherData,
                            byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();
            alg.Key = Key;
            alg.IV = IV;
            CryptoStream cs = new CryptoStream(ms,
                alg.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(cipherData, 0, cipherData.Length);
            cs.Close();
            byte[] decryptedData = ms.ToArray();
            return decryptedData;
        }
        #endregion

        #region WebrequestCall
        private string WebrequestCall(string inXML)
        {
            string outXML = "";
            string errMsg = "";
            try
            {
                EWSService.MyvrmReference.VRMNewWebService service = new EWSService.MyvrmReference.VRMNewWebService();
                service.Url = myVRMURL;
                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                    ((sender1, certificate, chain, sslPolicyErrors) => true);
                outXML = service.InvokeWebservice(inXML);

            }
            catch (System.Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                log.Trace(errMsg);
            }
            return outXML;
        }
        #endregion

        #region Check Room Availablity
        [WebMethod]
        public string CheckRoomAvailablity(String Inxml)
        {
            string errMsg = "";
            XmlDocument xml = null;
            string icalID = "", UID = "", conflictRooms = "";
            Int32 mins = 0;
            XmlNodeList LocResources = null, node = null;
            DateTime ConfDateTime = DateTime.Now;
            try
            {
                GetApplicationSettings();

                ExchangeServiceBinding();

                log.Trace("Checking Room Availability Starts ...");

                xml = new XmlDocument();
                xml.LoadXml(Inxml);
                node = xml.SelectNodes("//Conferences/Conference");
                for (int i = 0; i < node.Count; i++)
                {
                    if (node[i].SelectSingleNode("IcalId") != null)
                        icalID = node[i].SelectSingleNode("IcalId").InnerText;

                    if (node[i].SelectSingleNode("Duration") != null)
                        int.TryParse(node[i].SelectSingleNode("//Conferences/Conference/Duration").InnerText, out mins);

                    LocResources = node[i].SelectNodes("//Conferences/Conference/RoomQueues/RoomQueue");

                    if (node[i].SelectSingleNode("StartDate") != null)
                        DateTime.TryParse(node[i].SelectSingleNode("StartDate").InnerText, out ConfDateTime);

                    view = new Microsoft.Exchange.WebServices.Data.CalendarView(ConfDateTime, ConfDateTime.AddMinutes(mins));
                    view.StartDate = ConfDateTime.ToLocalTime();
                    view.EndDate = ConfDateTime.AddMinutes(mins).ToLocalTime();
                    view.Traversal = ItemTraversal.Shallow;
                    view.MaxItemsReturned = 100;// Works because max of 96 conference of 15 mins in one day
                    ExtPropertyDef = new ExtendedPropertyDefinition(DefaultExtendedPropertySet.Appointment, 0x8238, MapiPropertyType.String);

                    foreach (XmlNode LocResource in LocResources)
                    {
                        if (LocResource.InnerText != "")
                        {
                            appnts = service.FindAppointments(new FolderId(WellKnownFolderName.Calendar, LocResource.InnerText), view); // To Check appointment for the selected Date and time
                            if (appnts != null)
                            {
                                if (appnts.Items.Count > 0)
                                {
                                    service.LoadPropertiesForItems(from Item item in appnts select item,
                                                                 new PropertySet(BasePropertySet.FirstClassProperties,
                                                                    AppointmentSchema.ICalUid, AppointmentSchema.ICalRecurrenceId,
                                                                    ExtPropertyDef));
                                }
                                else
                                {
                                    log.Trace("No Conflict Found");
                                }
                            }
                            foreach (var item in appnts.Items)
                            {
                                UID = item.ICalUid.Trim();
                                if (UID != icalID)
                                {
                                    if (conflictRooms == "")
                                    {
                                        conflictRooms = "'" + LocResource.InnerText.Trim() + "'";
                                        log.Trace("Room Conflict :" + LocResource.InnerText.Trim());
                                    }
                                    else
                                    {
                                        conflictRooms += ", '" + LocResource.InnerText.Trim() + "'";
                                        log.Trace("Room Conflict :" + LocResource.InnerText.Trim());
                                    }
                                }
                            }
                        }
                    }
                }
                log.Trace("Checking Room Availability Ends ...");
            }
            catch (Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                log.Trace(errMsg);
                return errMsg;
            }
            return conflictRooms;
        }
        #endregion

        #region Create Conference Related Commands

        #region Create New Conference
        /// <summary>
        /// CreateConference
        /// </summary>
        /// <param name="createtable"></param>
        /// <returns></returns>
        [WebMethod]
        public DataSet CreateConference(DataSet ds)
        {
            string errMsg = "", confName = "", ICALID = "", RoomName = "", partyName = "";
            String HostEmail = "", partyEmail = "", RoomQueueEmail = "", standardTimeZone = "", confDescription = ""; //ZD 102287 //ALLDEV-863
            int rowid = 0, confid = 0, InstanceID = 0, duration = 0, confnumName = 0, IsRecurring = 0, exchangeMode = 0;//ZD 100924
            Appointment appt = null;
            List<AttendeeInfo> attendeeInfo = new List<AttendeeInfo>();
            List<AttendeeInfo> roomInfo = new List<AttendeeInfo>();
            DateTime StartDateTime = DateTime.Now;
            try
            {
                if (ds.Tables.Count <= 0)
                    return new DataSet();

                DataTable createtable = ds.Tables[0];

                GetApplicationSettings();

                if (!ExchangeServiceBinding())//Version Check
                {
                    log.Trace("Conference was not created Successfully.");
                    createtable.Rows[0]["Errid"] = "705";
                    return ds;
                }

                log.Trace("Create Conference Starts...");

                log.Trace("Create Conference Table Count : " + createtable.Rows.Count.ToString());

                if (createtable.Rows.Count > 0)
                {
                    for (int i = 0; i < createtable.Rows.Count; i++)
                    {
                        if (createtable.Rows[i]["RowUID"] != null)
                            Int32.TryParse(createtable.Rows[i]["RowUID"].ToString(), out rowid);
                        if (createtable.Rows[i]["ConfID"] != null)
                            Int32.TryParse(createtable.Rows[i]["ConfID"].ToString(), out confid);
                        if (createtable.Rows[i]["InstanceID"] != null)
                            Int32.TryParse(createtable.Rows[i]["InstanceID"].ToString(), out InstanceID);
                        if (createtable.Rows[i]["confnumName"] != null)
                            Int32.TryParse(createtable.Rows[i]["confnumName"].ToString(), out confnumName);
                        if (createtable.Rows[i]["confName"] != null)
                            confName = createtable.Rows[i]["confName"].ToString();
                        //ZD 101123 Start
                        if (createtable.Rows[i]["confStartDateTime"] != null)
                            StartDateTime = DateTime.Parse(createtable.Rows[i]["confStartDateTime"].ToString(), CultureInfo.GetCultureInfo("en-US"));
                        //ZD 101123 End
                        if (createtable.Rows[i]["Duration"] != null)
                            Int32.TryParse(createtable.Rows[i]["Duration"].ToString(), out duration);
                        if (createtable.Rows[i]["confHost"] != null)
                            HostEmail = createtable.Rows[i]["confHost"].ToString();
                        if (createtable.Rows[i]["IsRecurring"] != null)
                            Int32.TryParse(createtable.Rows[i]["IsRecurring"].ToString(), out IsRecurring);
                        if (createtable.Rows[i]["partyName"] != null)
                            partyName = createtable.Rows[i]["partyName"].ToString();
                        if (createtable.Rows[i]["partyEmail"] != null)
                            partyEmail = createtable.Rows[i]["partyEmail"].ToString();
                        if (createtable.Rows[i]["ICALID"] != null) //ZD 102723
                            ICALID = createtable.Rows[i]["ICALID"].ToString();
                        if (createtable.Rows[i]["exchangeMode"] != null) //ZD 100924
                            Int32.TryParse(createtable.Rows[i]["exchangeMode"].ToString(), out exchangeMode);
                        if (createtable.Rows[i]["standardTimeZone"] != null) //ZD 102287
                            standardTimeZone = createtable.Rows[i]["standardTimeZone"].ToString();
                        if (createtable.Rows[i]["confDescription"] != null) //ALLDEV-863
                            confDescription = createtable.Rows[i]["confDescription"].ToString();

                        if (partyEmail != "")
                        {
                            for (int j = 0; j < partyEmail.Split(':').Length; j++)
                            {
                                attendeeInfo.Add(partyEmail.Split(':')[j].ToString());
                            }
                        }

                        if (createtable.Rows[i]["RoomName"] != null)
                            RoomName = createtable.Rows[i]["RoomName"].ToString();

                        if (createtable.Rows[i]["RoomQueueEmail"] != null)
                            RoomQueueEmail = createtable.Rows[i]["RoomQueueEmail"].ToString();

                        if (RoomQueueEmail != "")
                        {
                            for (int k = 0; k < RoomQueueEmail.Split(':').Length; k++)
                            {
                                if (!string.IsNullOrEmpty(RoomQueueEmail.Split(':')[k]))//ZD 102472
                                    roomInfo.Add(RoomQueueEmail.Split(':')[k].ToString());      
                            }
                        }

                        if (StartDateTime <= DateTime.MinValue)
                        {
                            log.Trace("Invalid conference Date/Time");
                            createtable.Rows[i]["Errid"] = "223";
                        }

                        log.Trace("User Calendar Access Available : " + myVRMUserAccess);

                        if (exchangeMode == 1) //ZD 100924
                        {
                            if (myVRMUserAccess == "1")
                            {
                                if (!CreateConfUsingHost(confName, confid, InstanceID, confnumName, StartDateTime, duration, HostEmail, partyName, partyEmail, RoomName, RoomQueueEmail, appt, attendeeInfo, roomInfo, IsRecurring, standardTimeZone, ref ICALID, confDescription)) //ZD 102287 //ALLDEV-863
                                {
                                    log.Trace("Conference was not created Successfully.");
                                    createtable.Rows[i]["Errid"] = "703";
                                }
                            }
                            else
                            {
                                if (!CreateConfUsingImpersonate(confName, confid, InstanceID, confnumName, StartDateTime, duration, HostEmail, partyName, partyEmail, RoomName, RoomQueueEmail, appt, attendeeInfo, roomInfo, IsRecurring, standardTimeZone, ref ICALID, confDescription))//ZD 102287 //ALLDEV-863
                                {
                                    log.Trace("Conference was not created Successfully.");
                                    createtable.Rows[i]["Errid"] = "703";
                                }
                            }
                        }
                        else if (exchangeMode == 2) //ZD 100924
                        {
                            DateTime Endtime = StartDateTime.AddMinutes(duration);
                            InsertOneWayintoDB(0, confid, InstanceID, confnumName, ICALID, StartDateTime, Endtime, confName, RoomQueueEmail, HostEmail);
                        }

                        log.Trace("Conference Name :" + confName + " StartDate/Time : " + StartDateTime.ToString() + "ICALID : " + ICALID);

                        createtable.Rows[i]["ICALID"] = ICALID;
                    }
                }
                log.Trace("Create Conference Ends...");
                return ds;
            }

            catch (Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                log.Trace(errMsg);
                return new DataSet();
            }
        }
        #endregion

        #region Create Conference Using Host
        /// <summary>
        /// CreateConfUsingHost
        /// </summary>
        /// <param name="confName"></param>
        /// <param name="StartDateTime"></param>
        /// <param name="duration"></param>
        /// <param name="HostEmail"></param>
        /// <param name="partyEmail"></param>
        /// <param name="RoomQueueEmail"></param>
        /// <param name="appt"></param>
        /// <param name="Attendees"></param>
        /// <param name="Rooms"></param>
        /// <param name="ICALID"></param>
        /// <returns></returns>
        public bool CreateConfUsingHost(string confName, int confid, int instanceid, int Confnumname, DateTime StartDateTime, int duration, string HostEmail, string partyname, string partyEmail, string Roomname, string RoomQueueEmail, Appointment appt, List<AttendeeInfo> Attendees, List<AttendeeInfo> Rooms, int IsRecurring, string standardTimeZone, ref string ICALID, string Description) //ZD 102287 //ALLDEV-863
        {
            string errMsg = "", organizer = "", RoomItemIds = "", AttendeesItemIds = "", organizerItemID = "";
            try
            {
                log.Trace("Create Conference Using Organizer Email Starts ...");

                appt = new Appointment(service);
                appt.Subject = confName;
                appt.Body = Description.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;"); //ALLDEV-863
				//ZD 102287 starts
                TimeZoneInfo newTimeZone = TimeZoneInfo.Utc;
                try
                {
                    newTimeZone = TimeZoneInfo.FindSystemTimeZoneById(standardTimeZone);
                }
                catch (Exception e)
                {
                    log.Trace("Error in fetching TimeZoneInfo from standard timezone name: " + standardTimeZone + " error is: " + e.Message);
                    newTimeZone = TimeZoneInfo.Utc;
                }
                appt.StartTimeZone = newTimeZone;
                appt.EndTimeZone = newTimeZone;
                log.Trace("newTimeZone : " + newTimeZone);
                //appt.StartTimeZone = TimeZoneInfo.Utc;
				//ZD 102287 End
                appt.Start = StartDateTime;
                appt.End = appt.Start.AddMinutes(duration);
               
                log.Trace("Organizer EmailAddress : " + HostEmail);

                for (int i = 0; i < Attendees.Count; i++)
                {
                    appt.RequiredAttendees.Add(Attendees[i].SmtpAddress);
                }
                appt.Location = Roomname;
                for (int j = 0; j < Rooms.Count; j++)
                {
                    appt.Resources.Add(Rooms[j].SmtpAddress);
                }

                if (HostEmail != "")
                {

                    appt.Save(new FolderId(WellKnownFolderName.Calendar, HostEmail), SendInvitationsMode.SendToAllAndSaveCopy);

                    log.Trace("Conference Created Successfully");

                    log.Trace("Find Appointments Starts...");

                    string ID = appt.Id.ToString();
                    appt = Appointment.Bind(service, new ItemId(ID));

                    log.Trace("Appointment ICALID : " + appt.ICalUid.Trim());

                    ICALID = appt.ICalUid.Trim();

                    SearchFilter.SearchFilterCollection searchFilter = new SearchFilter.SearchFilterCollection();
                    searchFilter.Add(new SearchFilter.IsEqualTo(AppointmentSchema.Subject, appt.Subject));
                    searchFilter.Add(new SearchFilter.IsEqualTo(AppointmentSchema.Start, appt.Start));

                    ItemView Itemview = new ItemView(5);
                    Itemview.PropertySet = new PropertySet(BasePropertySet.IdOnly, AppointmentSchema.Subject, AppointmentSchema.Start, AppointmentSchema.End);

                    FindItemsResults<Item> OrganizerResults = service.FindItems(new FolderId(WellKnownFolderName.Calendar, HostEmail), searchFilter, Itemview);

                    foreach (var results in OrganizerResults)
                    {
                        organizer = HostEmail;
                        organizerItemID = results.Id.UniqueId;
                    }
                  

                    for (int l = 0; l < Rooms.Count; l++)
                    {
                        FindItemsResults<Item> RoomResults = service.FindItems(new FolderId(WellKnownFolderName.Calendar, Rooms[l].SmtpAddress), searchFilter, Itemview);

                        foreach (var results in RoomResults)
                        {
                            RoomItemIds += Rooms[l].SmtpAddress + ":" + results.Id.UniqueId + ";";
                        }
                    }

                    for (int m = 0; m < Attendees.Count; m++)
                    {
                        FindItemsResults<Item> AttendeeResults = service.FindItems(new FolderId(WellKnownFolderName.Calendar, Attendees[m].SmtpAddress), searchFilter, Itemview);

                        foreach (var results in AttendeeResults)
                        {
                            AttendeesItemIds += Attendees[m].SmtpAddress + ":" + results.Id.UniqueId + ";";
                        }
                    }

                    log.Trace("Find Appointments Ends...");
                    log.Trace("Insert into DB Starts...");
                    InsertintoDB(appt, 0, organizer,organizerItemID, confid.ToString(), instanceid.ToString(),confName, RoomItemIds, AttendeesItemIds,IsRecurring);
                    log.Trace("Insert into DB Ends...");
                }

                log.Trace("Create Conference Using Organizer Email Ends ...");

                return true;
            }

            catch (Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                log.Trace(errMsg);
                return false;
            }
        }
        #endregion

        #region Create Conference Using Impersonate
        /// <summary>
        /// CreateConfUsingICALID
        /// </summary>
        /// <param name="confName"></param>
        /// <param name="StartDateTime"></param>
        /// <param name="duration"></param>
        /// <param name="HostEmail"></param>
        /// <param name="partyEmail"></param>
        /// <param name="RoomQueueEmail"></param>
        /// <param name="appt"></param>
        /// <param name="Attendees"></param>
        /// <param name="Rooms"></param>
        /// <param name="ICALID"></param>
        /// <returns></returns>
        public bool CreateConfUsingImpersonate(string confName, int confid, int instanceid, int Confnumname, DateTime StartDateTime, int duration, string HostEmail, string partyname, string partyEmail, string Roomname, string RoomQueueEmail, Appointment appt, List<AttendeeInfo> Attendees, List<AttendeeInfo> Rooms, int IsRecurring, string standardTimeZone, ref string ICALID, string Description)//ZD 102287 //ALLDEV-863
        {
            string errMsg = "", RoomItemIds = "";
            try
            {
                log.Trace("Create Conference Using ICALID Starts ...");

                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, HostEmail);

                appt = new Appointment(service);
                appt.Subject = confName;
                appt.Body = Description.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;"); //ALLDEV-863
				//ZD 102287 Starts
                TimeZoneInfo newTimeZone = TimeZoneInfo.Utc;
                try
                {
                    newTimeZone = TimeZoneInfo.FindSystemTimeZoneById(standardTimeZone);
                }
                catch (Exception e)
                {
                    log.Trace("Error in fetching TimeZoneInfo from standard timezone name: " + standardTimeZone + " error is: " + e.Message);
                    newTimeZone = TimeZoneInfo.Utc;
                }
                log.Trace("newTimeZone : " + newTimeZone);
                appt.StartTimeZone = newTimeZone;
                appt.EndTimeZone = newTimeZone;
                //appt.StartTimeZone = TimeZoneInfo.Utc;
				//ZD 102287 End
                appt.Start = StartDateTime;
                appt.End = appt.Start.AddMinutes(duration);

                for (int i = 0; i < Attendees.Count; i++)
                {
                    appt.RequiredAttendees.Add(Attendees[i].SmtpAddress);
                }

                appt.Location = Roomname;

                for (int i = 0; i < Rooms.Count; i++)
                {
                    appt.Resources.Add(Rooms[i].SmtpAddress);
                }
                
                //if (Rooms.Count > 0) Commented for ZD 101255
                //{

                    appt.Save(new FolderId(WellKnownFolderName.Calendar, HostEmail), SendInvitationsMode.SendToAllAndSaveCopy);

                    log.Trace("Conference Created Successfully");

                    log.Trace("Find Appointments Starts...");

                    string ID = appt.Id.ToString();
                    appt = Appointment.Bind(service, new ItemId(ID));

                    log.Trace("Appointment ICALID : " + appt.ICalUid.Trim());

                    ICALID = appt.ICalUid.Trim();
                    /*
                    SearchFilter.SearchFilterCollection searchFilter = new SearchFilter.SearchFilterCollection();
                    searchFilter.Add(new SearchFilter.IsEqualTo(AppointmentSchema.Subject, appt.Subject));
                    searchFilter.Add(new SearchFilter.IsEqualTo(AppointmentSchema.Start, appt.Start));

                    ItemView Itemview = new ItemView(5);
                    Itemview.PropertySet = new PropertySet(BasePropertySet.IdOnly, AppointmentSchema.Subject, AppointmentSchema.Start, AppointmentSchema.End);

                    for (int l = 0; l < Rooms.Count; l++)
                    {
                        FindItemsResults<Item> RoomResults = service.FindItems(new FolderId(WellKnownFolderName.Calendar, Rooms[l].SmtpAddress), searchFilter, Itemview);

                        foreach (var results in RoomResults)
                        {
                            RoomItemIds += Rooms[l].SmtpAddress + ":" + results.Id.UniqueId + ";";
                        }
                    }
                    */
                    log.Trace("Find Appointments End...");
                //}
                InsertintoDB(appt, 0,"","", confid.ToString(), instanceid.ToString(), Confnumname.ToString(),RoomItemIds, "", IsRecurring);

                log.Trace("Create Conference Using ICALID Ends ...");

                return true;
            }

            catch (Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                log.Trace(errMsg);
                return false;
            }
        }
        #endregion

        #endregion

        #region Delete Conference Related Command

        #region Delete Conference
        /// <summary>
        /// DeleteConfUsingHost
        /// </summary>
        /// <param name="appt"></param>
        /// <param name="ICALID"></param>
        /// <param name="errid"></param>
        /// <returns></returns>
        [WebMethod]
        public bool DeleteConference(DataSet ds)
        {
            string errMsg = "", confName = "", ICALID = "", partyName = "", RoomName = "";
            String HostEmail = "", partyEmail = "", RoomQueueEmail = "";
            int rowid = 0, confid = 0, InstanceID = 0, duration = 0, confnumName = 0, IsRecurring = 0, exchangeMode = 0;//ZD 100924
            Appointment appt = null;
            List<AttendeeInfo> attendeeInfo = new List<AttendeeInfo>();
            List<AttendeeInfo> roomInfo = new List<AttendeeInfo>();
            DateTime StartDateTime = DateTime.Now;

            try
            {
                if (ds.Tables.Count <= 0)
                    return false;

                DataTable createtable = ds.Tables[0];

                GetApplicationSettings();

                if (!ExchangeServiceBinding())//Version Check
                {
                    log.Trace("Error in Checking Exchange Version.");
                    return false;
                }

                log.Trace("Delete Conference Starts...");
                log.Trace("Delete Conference Table Count : " + createtable.Rows.Count.ToString());

                if (createtable.Rows.Count > 0)
                {
                    for (int i = 0; i < createtable.Rows.Count; i++)
                    {
                        if (createtable.Rows[i]["RowUID"] != null)
                            Int32.TryParse(createtable.Rows[i]["RowUID"].ToString(), out rowid);
                        if (createtable.Rows[i]["ConfID"] != null)
                            Int32.TryParse(createtable.Rows[i]["ConfID"].ToString(), out confid);
                        if (createtable.Rows[i]["InstanceID"] != null)
                            Int32.TryParse(createtable.Rows[i]["InstanceID"].ToString(), out InstanceID);
                        if (createtable.Rows[i]["confnumName"] != null)
                            Int32.TryParse(createtable.Rows[i]["confnumName"].ToString(), out confnumName);
                        if (createtable.Rows[i]["confName"] != null)
                            confName = createtable.Rows[i]["confName"].ToString();
                        //ZD 101123 Start
                        if (createtable.Rows[i]["confStartDateTime"] != null)
                            StartDateTime = DateTime.Parse(createtable.Rows[i]["confStartDateTime"].ToString(), CultureInfo.GetCultureInfo("en-US"));
                        //ZD 101123 End
                        if (createtable.Rows[i]["Duration"] != null)
                            Int32.TryParse(createtable.Rows[i]["Duration"].ToString(), out duration);
                        if (createtable.Rows[i]["confHost"] != null)
                            HostEmail = createtable.Rows[i]["confHost"].ToString();
                        if (createtable.Rows[i]["IsRecurring"] != null)
                            Int32.TryParse(createtable.Rows[i]["IsRecurring"].ToString(), out IsRecurring);
                        if (createtable.Rows[i]["partyName"] != null)
                            partyName = createtable.Rows[i]["partyName"].ToString();
                        if (createtable.Rows[i]["partyEmail"] != null)
                            partyEmail = createtable.Rows[i]["partyEmail"].ToString();
                        if (createtable.Rows[i]["ICALID"] != null)
                            ICALID = createtable.Rows[i]["ICALID"].ToString();
                        if (createtable.Rows[i]["exchangeMode"] != null) //ZD 100924
                            Int32.TryParse(createtable.Rows[i]["exchangeMode"].ToString(), out exchangeMode);

                        for (int j = 0; j < partyEmail.Split(':').Length - 1; j++)
                        {
                            attendeeInfo.Add(partyEmail.Split(':')[i].ToString());
                        }

                        if (createtable.Rows[i]["RoomName"] != null)
                            RoomName = createtable.Rows[i]["RoomName"].ToString();

                        if (createtable.Rows[i]["RoomQueueEmail"] != null)
                            RoomQueueEmail = createtable.Rows[i]["RoomQueueEmail"].ToString();

                        for (int k = 0; k < RoomQueueEmail.Split(':').Length - 1; k++)
                        {
                            if (!string.IsNullOrEmpty(RoomQueueEmail.Split(':')[k]))//ZD 102472
                                roomInfo.Add(RoomQueueEmail.Split(':')[k].ToString());      
                        }

                        if (StartDateTime <= DateTime.MinValue)
                        {
                            log.Trace("Invalid conference Date/Time");
                            createtable.Rows[i]["Errid"] = "241";
                        }

                        if (exchangeMode == 1)
                        {
                            if (ICALID != "") //ZD 101123
                            {
                                if (!DeleteConfUsingImpersonate(confName, confid, InstanceID, confnumName, StartDateTime, duration, HostEmail, partyName, partyEmail, RoomName, RoomQueueEmail, appt, attendeeInfo, roomInfo, IsRecurring, ref ICALID))
                                {
                                    log.Trace("Error in Deleting the Conference");
                                    createtable.Rows[i]["Errid"] = "703";
                                }
                            }
                        }
                        else if (exchangeMode == 2) //ZD 100924
                        {
                            DateTime Endtime = StartDateTime.AddMinutes(duration);
                            UpdateOneWayintoDB(1, confid, InstanceID, confnumName, ICALID, StartDateTime, Endtime, confName, RoomQueueEmail, HostEmail);
                        }

                        log.Trace("Conference Name :" + confName + " StartDate/Time : " + StartDateTime.ToString() + "ICALID : " + ICALID);

                        createtable.Rows[i]["ICALID"] = ICALID;
                    }
                }

                log.Trace("Delete Conference Ends...");

                return true;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                log.Trace(errMsg);
                return false;
            }
        }
        #endregion

        #region  Delete Conference Using Impersonate
        /// <summary>
        /// DeleteConference
        /// </summary>
        /// <param name="ICALID"></param>
        /// <param name="errid"></param>
        /// <returns></returns>

        public bool DeleteConfUsingImpersonate(string confName, int confid, int instanceid, int Confnumname, DateTime StartDateTime, int duration, string HostEmail, string partyname, string partyEmail, string Roomname, string RoomQueueEmail, Appointment appt, List<AttendeeInfo> Attendees, List<AttendeeInfo> Rooms, int IsRecurring, ref string ICALID)
        {
            string errMsg = "", RoomItemIds = "";
            bool Appointcheck = false;
            Appointment appointment = null;
            DateTime confdatetime = DateTime.MinValue;
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand mycommand = new SqlCommand();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(mycommand);
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataTable table = new DataTable();
            try
            {
                
                log.Trace("Delete Conference Starts ...");

                mycommand.Connection = myConnection;
                mycommand.CommandText = "SELECT distinct convert(datetime,startdate) as dtTime from ConferenceCreate where ConfID = " + confid + " and instanceID = " + instanceid;
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                dataAdapter.Fill(table);
                dataAdapter.Dispose();

                if (table != null)
                {
                    if (table.Rows.Count > 0)
                    {
                        for (int cnt = 0; cnt < table.Rows.Count; cnt++)
                        {
                            confdatetime = DateTime.MinValue;
                            if (!DateTime.TryParse(table.Rows[cnt]["dtTime"].ToString(), out confdatetime))
                                confdatetime = DateTime.ParseExact(table.Rows[cnt]["dtTime"].ToString(), "dd/MM/yyyy HH:mm:ss", null);

                        }
                    }
                }

                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, HostEmail);
               
                ItemView Itemview = new ItemView(52);
                Itemview.PropertySet = new PropertySet(BasePropertySet.IdOnly, AppointmentSchema.Subject, AppointmentSchema.Start, AppointmentSchema.End, AppointmentSchema.ICalUid, AppointmentSchema.Location, AppointmentSchema.LastModifiedTime, AppointmentSchema.AppointmentState, AppointmentSchema.Id);
                               
                FindItemsResults<Item> RoomResults = service.FindItems(new FolderId(WellKnownFolderName.Calendar, HostEmail), Itemview);

                foreach (var results in RoomResults)
                {
                    appt = (Appointment)results;
                    if (appt.ICalUid.Trim() == ICALID.Trim().Split('_')[0])
                    {
                        Appointcheck = true;

                        appointment = Appointment.Bind(service, new ItemId(appt.Id.UniqueId));

                        if (appointment.AppointmentType == AppointmentType.RecurringMaster)
                        {
                            int iCurrentIndex = 1;
                            try
                            {
                                while (true)
                                {
                                    appt = Appointment.BindToOccurrence(service, new ItemId(appointment.Id.UniqueId), iCurrentIndex);

                                    if (appt.Start.Date == confdatetime.Date)
                                        break;

                                    iCurrentIndex++;
                                }
                            }
                            catch (Exception e)
                            {
                                if (e.Message == "Occurrence index is out of recurrence range.")
                                {
                                    Console.WriteLine("The end of the series has been reached.");
                                }
                                else
                                {
                                    throw e;
                                }
                            }
                        }
                        else
                            break;
                    }
                    if (Appointcheck)
                        break;
                }

                appt.CancelMeeting("Deleted from myVRM");
                
                log.Trace("Delete Appointments End...");

                UpdateintoDB(appt, 1, "", "", confid.ToString(), instanceid.ToString(), Confnumname.ToString(), RoomItemIds, "", IsRecurring, ICALID);

                log.Trace("Delete Conference Ends ...");

                return true;
            }

            catch (Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                log.Trace(errMsg);
                return false;
            }
        }
        #endregion

        #endregion

        #region Update Conference Related Command

        #region Update Conference
        /// <summary>
        /// UpdateConference
        /// </summary>
        /// <param name="createtable"></param>
        /// <param name="errid"></param>
        /// <returns></returns>
        [WebMethod]
        public DataSet UpdateConference(DataSet ds)
        {
            string errMsg = "", confName = "", ICALID = "", partyName = "", RoomName = "";
            String HostEmail = "", partyEmail = "", RoomQueueEmail = "", standardTimeZone = "", confDescription = ""; //ZD 102287 //ALLDEV-863
            int rowid = 0, confid = 0, InstanceID = 0, duration = 0, confnumName = 0, IsRecurring = 0, exchangeMode = 0;//ZD 100924
            Appointment appt = null;
            List<AttendeeInfo> attendeeInfo = new List<AttendeeInfo>();
            List<AttendeeInfo> roomInfo = new List<AttendeeInfo>();
            DateTime StartDateTime = DateTime.Now;

            try
            {
                if (ds.Tables.Count <= 0)
                    return new DataSet();

                DataTable createtable = ds.Tables[0];

                GetApplicationSettings();


                if (!ExchangeServiceBinding())//Version Check
                {
                    log.Trace("Conference was not created Successfully.");
                    createtable.Rows[0]["Errid"] = "705";
                    return ds;
                }

                log.Trace("Update Conference Starts...");
                log.Trace("Update Conference Table Count : " + createtable.Rows.Count.ToString());

                if (createtable.Rows.Count > 0)
                {
                    for (int i = 0; i < createtable.Rows.Count; i++)
                    {
                        if (createtable.Rows[i]["RowUID"] != null)
                            Int32.TryParse(createtable.Rows[i]["RowUID"].ToString(), out rowid);
                        if (createtable.Rows[i]["ConfID"] != null)
                            Int32.TryParse(createtable.Rows[i]["ConfID"].ToString(), out confid);
                        if (createtable.Rows[i]["InstanceID"] != null)
                            Int32.TryParse(createtable.Rows[i]["InstanceID"].ToString(), out InstanceID);
                        if (createtable.Rows[i]["confnumName"] != null)
                            Int32.TryParse(createtable.Rows[i]["confnumName"].ToString(), out confnumName);
                        if (createtable.Rows[i]["confName"] != null)
                            confName = createtable.Rows[i]["confName"].ToString();
                        //ZD 101123 Start
                        if (createtable.Rows[i]["confStartDateTime"] != null)
                            StartDateTime = DateTime.Parse(createtable.Rows[i]["confStartDateTime"].ToString(), CultureInfo.GetCultureInfo("en-US"));
                        //ZD 101123 End
                        if (createtable.Rows[i]["Duration"] != null)
                            Int32.TryParse(createtable.Rows[i]["Duration"].ToString(), out duration);
                        if (createtable.Rows[i]["confHost"] != null)
                            HostEmail = createtable.Rows[i]["confHost"].ToString();
                        if (createtable.Rows[i]["IsRecurring"] != null)
                            Int32.TryParse(createtable.Rows[i]["IsRecurring"].ToString(), out IsRecurring);
                        if (createtable.Rows[i]["partyName"] != null)
                            partyName = createtable.Rows[i]["partyName"].ToString();
                        if (createtable.Rows[i]["partyEmail"] != null)
                            partyEmail = createtable.Rows[i]["partyEmail"].ToString();
                        if (createtable.Rows[i]["ICALID"] != null)
                            ICALID = createtable.Rows[i]["ICALID"].ToString();
                        if (createtable.Rows[i]["exchangeMode"] != null) //ZD 100924
                            Int32.TryParse(createtable.Rows[i]["exchangeMode"].ToString(), out exchangeMode);
                        if (createtable.Rows[i]["standardTimeZone"] != null) //ZD 102287
                            standardTimeZone = createtable.Rows[i]["standardTimeZone"].ToString();
                        if (createtable.Rows[i]["confDescription"] != null)  //ALLDEV-863
                            confDescription = createtable.Rows[i]["confDescription"].ToString();

                        if (partyEmail != "")
                        {
                            for (int j = 0; j < partyEmail.Split(':').Length; j++)
                            {
                                attendeeInfo.Add(partyEmail.Split(':')[j].ToString());
                            }
                        }
                        if (createtable.Rows[i]["RoomName"] != null)
                            RoomName = createtable.Rows[i]["RoomName"].ToString();

                        if (createtable.Rows[i]["RoomQueueEmail"] != null)
                            RoomQueueEmail = createtable.Rows[i]["RoomQueueEmail"].ToString();

                        if (RoomQueueEmail != "")
                        {
                            for (int k = 0; k < RoomQueueEmail.Split(':').Length; k++)
                            {
                                if (!string.IsNullOrEmpty(RoomQueueEmail.Split(':')[k]))//ZD 102472
                                    roomInfo.Add(RoomQueueEmail.Split(':')[k].ToString());      
                            }
                        }
                        if (StartDateTime <= DateTime.MinValue)
                        {
                            log.Trace("Invalid conference Date/Time");
                            createtable.Rows[i]["Errid"] = "241";
                        }
                        //ZD 101123 Start
                        if (exchangeMode == 1)
                        {
                            if (ICALID != "")
                            {
                                if (!UpdateConfUsingImpersonate(confName, confid, InstanceID, confnumName, StartDateTime, duration, HostEmail, partyName, partyEmail, RoomName, RoomQueueEmail, appt, attendeeInfo, roomInfo, IsRecurring, standardTimeZone, ref ICALID, confDescription))//ZD 102287 //ALLDEV-863
                                {
                                    log.Trace("Conference was not created Successfully.");
                                    createtable.Rows[i]["Errid"] = "703";
                                }
                            }
                            else
                            {
                                if (!CreateConfUsingImpersonate(confName, confid, InstanceID, confnumName, StartDateTime, duration, HostEmail, partyName, partyEmail, RoomName, RoomQueueEmail, appt, attendeeInfo, roomInfo, IsRecurring, standardTimeZone, ref ICALID, confDescription)) //ZD 102287 //ALLDEV-863
                                {
                                    log.Trace("Conference was not created Successfully.");
                                    createtable.Rows[i]["Errid"] = "703";
                                }
                            }
                        }
                        else if (exchangeMode == 2) //ZD 100924
                        {
                            DateTime Endtime = StartDateTime.AddMinutes(duration);
                            UpdateOneWayintoDB(0, confid, InstanceID, confnumName, ICALID, StartDateTime, Endtime, confName, RoomQueueEmail, HostEmail);
                        }

                        //ZD 101123 End
                        log.Trace("Conference Name :" + confName + " StartDate/Time : " + StartDateTime.ToString() + "ICALID : " + ICALID);

                        createtable.Rows[i]["ICALID"] = ICALID;
                    }
                }

                log.Trace("Update Conference Ends...");

                return ds;
            }
            catch (Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                log.Trace(errMsg);
                return null;
            }
        }

        #endregion

        #region Update Conference Using Host
        /// <summary>
        /// UpdateConfUsingHost
        /// </summary>
        /// <param name="confName"></param>
        /// <param name="StartDateTime"></param>
        /// <param name="duration"></param>
        /// <param name="HostEmail"></param>
        /// <param name="partyEmail"></param>
        /// <param name="RoomQueueEmail"></param>
        /// <param name="appt"></param>
        /// <param name="Attendees"></param>
        /// <param name="Rooms"></param>
        /// <param name="ICALID"></param>
        /// <returns></returns>
        public bool UpdateConfUsingHost(string confName, int confid, int instanceid, int confnumname, DateTime StartDateTime, int duration, string HostEmail, string Partyname, string partyEmail, string Roomname, string RoomQueueEmail, Appointment appt, List<AttendeeInfo> Attendees, List<AttendeeInfo> Rooms, int IsRecurring, string standardTimeZone, ref string ICALID, string confDescription)//ZD 102287 //ALLDEV-863
        {
            EWSService.ExchangeReference.ResolveNamesType Bindtype = null;
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand mycommand = null;
            SqlDataAdapter dataAdapter = null;
            DataTable table = null;
            string itemids = null;
            string errMsg = "", RoomItemIds = "", AttendeesItemIds = "", organizer = "", organizerItemID = "";
            try
            {

                log.Trace("Update Conference Using Organizer Email Starts ...");

                table = new DataTable();
                mycommand = new SqlCommand();
                mycommand.Connection = myConnection;
                mycommand.CommandText = "SELECT organizer,organizerItemID from ConferenceCreate Where ICALID ='" + ICALID + "'";
                table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                dataAdapter = new SqlDataAdapter(mycommand);
                dataAdapter.Fill(table);
                dataAdapter.Dispose();
                if (table.Rows.Count == 0)
                    return true;
                else
                {
                    Bindtype = new EWSService.ExchangeReference.ResolveNamesType();

                    foreach (DataRow row in table.Rows)
                    {
                        itemids = null;

                        if (row["organizerItemID"].ToString() != "")
                            itemids = row["organizerItemID"].ToString();

                        log.Trace("Updating Conference ...");

                        appt = Appointment.Bind(service, new ItemId(itemids));

                        appt.Subject = confName;
                        appt.Body = confDescription.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;"); //ALLDEV-863
						//ZD 102287 Starts
                        TimeZoneInfo newTimeZone = TimeZoneInfo.Utc;
                        try
                        {
                            newTimeZone = TimeZoneInfo.FindSystemTimeZoneById(standardTimeZone);
                        }
                        catch (Exception e)
                        {
                            log.Trace("Error in fetching TimeZoneInfo from standard timezone name: " + standardTimeZone + " error is: " + e.Message);
                            newTimeZone = TimeZoneInfo.Utc;
                        }
                		log.Trace("newTimeZone : " + newTimeZone);
                        appt.StartTimeZone = newTimeZone;
                        appt.EndTimeZone = newTimeZone;
                        //appt.StartTimeZone = TimeZoneInfo.Utc;
						//ZD 102287 End
                        appt.Start = StartDateTime;
                        appt.End = appt.Start.AddMinutes(duration);

                        for (int i = 0; i < Attendees.Count; i++)
                        {
                            appt.RequiredAttendees.Add(Attendees[i].SmtpAddress); //Doubt
                            appt.Resources.Add(Attendees[i].SmtpAddress);
                        }
                        appt.Location = Roomname;
                        for (int j = 0; j < Rooms.Count; j++)
                        {
                            appt.Resources.Add(Rooms[j].SmtpAddress);
                        }

                        appt.Update(ConflictResolutionMode.AlwaysOverwrite, SendInvitationsOrCancellationsMode.SendOnlyToAll);
                        appt.Accept(true);//Auto accept Room 
                        log.Trace("Conference Updated Successfully");

                        ICALID = appt.ICalUid;

                        SearchFilter.SearchFilterCollection searchFilter = new SearchFilter.SearchFilterCollection();
                        searchFilter.Add(new SearchFilter.IsEqualTo(AppointmentSchema.Subject, appt.Subject));
                        searchFilter.Add(new SearchFilter.IsEqualTo(AppointmentSchema.Start, appt.Start));

                        ItemView Itemview = new ItemView(5);
                        Itemview.PropertySet = new PropertySet(BasePropertySet.IdOnly, AppointmentSchema.Subject, AppointmentSchema.Start, AppointmentSchema.End);

                        FindItemsResults<Item> OrganizerResults = service.FindItems(new FolderId(WellKnownFolderName.Calendar, HostEmail), searchFilter, Itemview);

                        foreach (var results in OrganizerResults)
                        {
                            organizer = HostEmail;
                            organizerItemID = results.Id.UniqueId;
                        }
                        
                        for (int l = 0; l < Rooms.Count; l++)
                        {
                            FindItemsResults<Item> RoomResults = service.FindItems(new FolderId(WellKnownFolderName.Calendar, Rooms[l].SmtpAddress), searchFilter, Itemview);

                            foreach (var results in RoomResults)
                            {
                                RoomItemIds += Rooms[l].SmtpAddress + ":" + results.Id.UniqueId + ";";
                            }
                        }

                        for (int m = 0; m < Attendees.Count; m++)
                        {
                            FindItemsResults<Item> AttendeeResults = service.FindItems(new FolderId(WellKnownFolderName.Calendar, Attendees[m].SmtpAddress), searchFilter, Itemview);

                            foreach (var results in AttendeeResults)
                            {
                                AttendeesItemIds += Attendees[m].SmtpAddress + ":" + results.Id.UniqueId + ";";
                            }
                        }
                        log.Trace("Update into DB Starts...");
                        UpdateintoDB(appt, 0, organizer,organizerItemID, confid.ToString(), instanceid.ToString(),confnumname.ToString(), RoomItemIds, AttendeesItemIds,IsRecurring, ICALID);
                        log.Trace("Update into DB Starts...");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                log.Trace(errMsg);
                return false;
            }
            return true;
        }

        #endregion

        #region Update Conference Using Impersonate
        /// <summary>
        /// CreateConfUsingICALID
        /// </summary>
        /// <param name="confName"></param>
        /// <param name="StartDateTime"></param>
        /// <param name="duration"></param>
        /// <param name="HostEmail"></param>
        /// <param name="partyEmail"></param>
        /// <param name="RoomQueueEmail"></param>
        /// <param name="appt"></param>
        /// <param name="Attendees"></param>
        /// <param name="Rooms"></param>
        /// <param name="ICALID"></param>
        /// <returns></returns>
        public bool UpdateConfUsingImpersonate(string confName, int confid, int instanceid, int Confnumname, DateTime StartDateTime, int duration, string HostEmail, string partyname, string partyEmail, string Roomname, string RoomQueueEmail, Appointment appt, List<AttendeeInfo> Attendees, List<AttendeeInfo> Rooms, int IsRecurring, string standardTimeZone, ref string ICALID ,string confDescription) //ZD 102287
        {
            string errMsg = "", RoomItemIds = "";
            Appointment appointment = null;
            bool Appointcheck = false;
            DateTime confdatetime = DateTime.MinValue;
            SqlConnection myConnection = new SqlConnection(connectionString);
            SqlCommand mycommand = new SqlCommand();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(mycommand);
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(dataAdapter);
            DataTable table = new DataTable();

            try
            {
                //if (Rooms.Count > 0) Commented for ZD 101255
                //{
                    log.Trace("Update Conference Starts ...");
                    
                    mycommand.Connection = myConnection;
                    mycommand.CommandText = "SELECT distinct convert(datetime,startdate) as dtTime from ConferenceCreate where ConfID = " + confid + " and instanceID = " + instanceid;
                    table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                    dataAdapter.Fill(table);
                    dataAdapter.Dispose();

                    if (table != null)
                    {
                        if (table.Rows.Count > 0)
                        {
                            for (int cnt = 0; cnt < table.Rows.Count; cnt++)
                            {
                                confdatetime = DateTime.MinValue;
                                if (!DateTime.TryParse(table.Rows[cnt]["dtTime"].ToString(), out confdatetime))
                                    confdatetime = DateTime.ParseExact(table.Rows[cnt]["dtTime"].ToString(), "dd/MM/yyyy HH:mm:ss", null);

                            }
                        }
                    }

                    service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, HostEmail);

                    ItemView Itemview = new ItemView(52);
                    Itemview.PropertySet = new PropertySet(BasePropertySet.IdOnly, AppointmentSchema.Subject, AppointmentSchema.Start, AppointmentSchema.End, AppointmentSchema.ICalUid,AppointmentSchema.Location,AppointmentSchema.LastModifiedTime,AppointmentSchema.AppointmentState,AppointmentSchema.Id);

                    FindItemsResults<Item> RoomResults = service.FindItems(new FolderId(WellKnownFolderName.Calendar, HostEmail), Itemview);

                    foreach (var results in RoomResults)
                    {
                        appt = (Appointment)results;
                        if (appt.ICalUid.Trim() == ICALID.Trim().Split('_')[0])
                        {
                            Appointcheck = true;

                            appointment = Appointment.Bind(service, new ItemId(appt.Id.UniqueId));

                            if (appointment.AppointmentType == AppointmentType.RecurringMaster)
                            {
                                int iCurrentIndex = 1;
                                try
                                {
                                    while (true)
                                    {
                                        appt = Appointment.BindToOccurrence(service, new ItemId(appointment.Id.UniqueId), iCurrentIndex);

                                        if (appt.Start.Date == confdatetime.Date)
                                            break;
                                      
                                        iCurrentIndex++;
                                    }
                                }
                                catch (Exception e)
                                {
                                    if (e.Message == "Occurrence index is out of recurrence range.")
                                    {
                                        Console.WriteLine("The end of the series has been reached.");
                                    }
                                    else
                                    {
                                        throw e;
                                    }
                                }
                            }
                            else
                                break;
                        }
                        if (Appointcheck)
                            break;
                    }

                    appt.Subject = confName;
                    appt.Body = confDescription.Replace("&", "	&amp;").Replace("<", "&lt;").Replace(">", "&gt;"); //ALLDEV-863
					//ZD 102287 Starts
                    TimeZoneInfo newTimeZone = TimeZoneInfo.Utc;
                    try
                    {
                        newTimeZone = TimeZoneInfo.FindSystemTimeZoneById(standardTimeZone);
                    }
                    catch (Exception e)
                    {
                        log.Trace("Error in fetching TimeZoneInfo from standard timezone name: " + standardTimeZone + " error is: " + e.Message);
                        newTimeZone = TimeZoneInfo.Utc;
                    }
                	log.Trace("newTimeZone : " + newTimeZone);
                    appt.StartTimeZone = newTimeZone;
                    appt.EndTimeZone = newTimeZone;
                    //appt.StartTimeZone = TimeZoneInfo.Utc;
                    //appt.EndTimeZone = TimeZoneInfo.Utc;
					//ZD 102287 End
                    appt.Start = StartDateTime;
                    appt.End = appt.Start.AddMinutes(duration);

                    for (int i = 0; i < Attendees.Count; i++)
                    {
                        appt.RequiredAttendees.Add(Attendees[i].SmtpAddress);
                    }

                    appt.Location = Roomname;

                    for (int i = 0; i < Rooms.Count; i++)
                    {
                        appt.Resources.Add(Rooms[i].SmtpAddress);
                    }

                    appt.Update(ConflictResolutionMode.AlwaysOverwrite, SendInvitationsOrCancellationsMode.SendToAllAndSaveCopy);

                    //ZD 101255 Start
                    log.Trace("Find Appointments Starts...");

                    string ID = appt.Id.ToString();
                    appt = Appointment.Bind(service, new ItemId(ID));
                    
                    log.Trace("Find Appointments Starts...");
                    //ZD 101255 Ends

                    UpdateintoDB(appt, 0, "", "", confid.ToString(), instanceid.ToString(), Confnumname.ToString(), RoomItemIds, "", IsRecurring, ICALID);
                    
                    log.Trace("Update Conference Ends ...");
                //}

                return true;
            }

            catch (Exception ex)
            {
                errMsg = ex.Message + "_CURRENTDATETIME:" + DateTime.Now.ToLongDateString();
                log.Trace(errMsg);
                return false;
            }
        }
        #endregion

        #endregion

        #region Database Updation

        #region Insert into DB
        /// <summary>
        /// InsertintoDB
        /// </summary>
        private void InsertintoDB(Appointment appt, int isCancelled, string Organizer, string OrganizerItemid, string confid, string instanceID, string confnumname, string RoomitemID, string Attendeeitemid, int IsRecurring)
        {
            try
            {
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                SqlCommand com = new SqlCommand(("INSERT INTO ConferenceCreate (confID ,instanceID,ConfNumName, Subject , confStatus , StartDate , EndDate ,IsRecurring, Location , organizer ,organizerItemID, isCancelled ,inXML,outXML,changeKey,LastModified,ICALID,AttendeeitemID,LocationitemID,isExchangeConf,ErrorFetchCount) VALUES ('" + confid + "','" + instanceID + "','" + confnumname + "','" + appt.Subject.Replace("'", "") + "'," + 3 + ",'" + appt.Start.ToString("dd MMMM yyyy hh:mm") + "','" + appt.End.ToString("dd MMMM yyyy hh:mm") + "'," + IsRecurring + ",'" + appt.Location + "','" + Organizer + "','" + OrganizerItemid + "'," + isCancelled + ",'','','" + appt.Id.ChangeKey + "','" + appt.LastModifiedTime.ToString("MM/dd/yyyy hh:mm tt") + "','" + appt.ICalUid + "','" + Attendeeitemid + "','" + RoomitemID + "',0,0)"), con);//103314
                com.ExecuteNonQuery();
                con.Close();
                con.Open();
                com = new SqlCommand(("INSERT INTO ConferenceCreateAudit (confID ,instanceID,ConfNumName, Subject , confStatus , StartDate , EndDate ,IsRecurring, Location , organizer ,organizerItemID, isCancelled ,inXML,outXML,changeKey,LastModified,ICALID,AttendeeitemID,LocationitemID,isExchangeConf,ErrorFetchCount) VALUES ('" + confid + "','" + instanceID + "','" + confnumname + "','" + appt.Subject.Replace("'", "") + "'," + 3 + ",'" + appt.Start.ToString("dd MMMM yyyy hh:mm") + "','" + appt.End.ToString("dd MMMM yyyy hh:mm") + "'," + IsRecurring + ",'" + appt.Location + "','" + Organizer + "','" + OrganizerItemid + "'," + isCancelled + ",'','','" + appt.Id.ChangeKey + "','" + appt.LastModifiedTime.ToString("MM/dd/yyyy hh:mm tt") + "','" + appt.ICalUid + "','" + Attendeeitemid + "','" + RoomitemID + "',0,0)"), con);//103314
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Update into DB
        private void UpdateintoDB(Appointment appt, int isCancelled, string Organizer, string OrganizerItemID, string confid, string instanceID,string confnumname, string RoomitemID, string Attendeeitemid,int IsRecurring, string ICALID)
        {
            try
            {
                if (isCancelled == 1)
                {
                    SqlConnection con = new SqlConnection(connectionString);
                    con.Open();
                    SqlCommand com = new SqlCommand(("UPDATE ConferenceCreate Set isCancelled = 1 Where confID = '" + confid + "' and ICALID = '" + ICALID + "'"), con);
                    com.ExecuteNonQuery();
                    con.Close();
                    con.Open();
                    com = new SqlCommand(("INSERT INTO ConferenceCreateAudit (confID ,instanceID,ConfNumName, Subject , confStatus , StartDate , EndDate ,IsRecurring, Location , organizer ,organizerItemID, isCancelled ,inXML,outXML,changeKey,LastModified,ICALID,AttendeeitemID,LocationitemID) VALUES ('" + confid + "','" + instanceID + "','" + confnumname + "','" + appt.Subject.Replace("'", "") + "'," + 3 + ",'" + appt.Start.ToString("MM/dd/yyyy hh:mm tt") + "','" + appt.End.ToString("MM/dd/yyyy hh:mm tt") + "'," + IsRecurring + ",'" + appt.Location + "','" + Organizer + "','" + OrganizerItemID + "'," + isCancelled + ",'','','" + appt.Id.ChangeKey + "','" + appt.LastModifiedTime.ToString("MM/dd/yyyy hh:mm tt") + "','" + appt.ICalUid + "','" + Attendeeitemid + "','" + RoomitemID + "')"), con);
                    com.ExecuteNonQuery();
                    con.Close();
                }
                else
                {
                    SqlConnection con = new SqlConnection(connectionString);
                    con.Open();
                    //ZD 102085 Starts
                    string sqlQry = "";
                    if (confid != "" && confid != "0,0")
                        sqlQry = "UPDATE ConferenceCreate Set ErrorFetchCount = 0, StartDate ='" + appt.Start.ToString("dd MMMM yyyy hh:mm") + "',inXML='',outXML='',EndDate='" + appt.End.ToString("dd MMMM yyyy hh:mm") + "',Subject='" + appt.Subject.Replace("'", "") + "',Location='" + appt.Location + "',isCancelled='" + isCancelled + "',changeKey='" + appt.Id.ChangeKey + "',LastModified = '" + appt.LastModifiedTime.ToString("MM/dd/yyyy hh:mm tt") + "',confID='" + confid + "',instanceID='" + instanceID + "',AttendeeitemID='" + Attendeeitemid + "',ConfNumName='" + confnumname + "',LocationitemID='" + RoomitemID + "' Where ICALID ='" + ICALID + "'";//103314
                    else
                        sqlQry = "UPDATE ConferenceCreate Set ErrorFetchCount = 0, StartDate ='" + appt.Start.ToString("dd MMMM yyyy hh:mm") + "',inXML='',outXML='',EndDate='" + appt.End.ToString("dd MMMM yyyy hh:mm") + "',Subject='" + appt.Subject.Replace("'", "") + "',Location='" + appt.Location + "',isCancelled='" + isCancelled + "',changeKey='" + appt.Id.ChangeKey + "',LastModified = '" + appt.LastModifiedTime.ToString("MM/dd/yyyy hh:mm tt") + "',AttendeeitemID='" + Attendeeitemid + "',ConfNumName='" + confnumname + "',LocationitemID='" + RoomitemID + "' Where ICALID ='" + ICALID + "'";//103314
                    //SqlCommand com = new SqlCommand(("UPDATE ConferenceCreate Set StartDate ='" + appt.Start.ToString("dd MMMM yyyy hh:mm") + "',inXML='',outXML='',EndDate='" + appt.End.ToString("dd MMMM yyyy hh:mm") + "',Subject='" + appt.Subject.Replace("'", "") + "',Location='" + appt.Location + "',isCancelled='" + isCancelled + "',changeKey='" + appt.Id.ChangeKey + "',LastModified = '" + appt.LastModifiedTime.ToString("MM/dd/yyyy hh:mm tt") + "',confID='" + confid + "',instanceID='" + instanceID + "',AttendeeitemID='" + Attendeeitemid + "',ConfNumName='" + confnumname + "',LocationitemID='" + RoomitemID + "' Where ICALID ='" + ICALID + "'"), con); //confStatus=" + appt.AppointmentState + ",
                    SqlCommand com = new SqlCommand((sqlQry), con); //confStatus=" + appt.AppointmentState + ", //ZD 102085 Ends
                    com.ExecuteNonQuery();
                    con.Close();
                    con.Open();
                    com = new SqlCommand(("INSERT INTO ConferenceCreateAudit (confID ,instanceID,ConfNumName, Subject , confStatus , StartDate , EndDate ,IsRecurring, Location , organizer ,organizerItemID, isCancelled ,inXML,outXML,changeKey,LastModified,ICALID,AttendeeitemID,LocationitemID) VALUES ('" + confid + "','" + instanceID + "','" + confnumname + "','" + appt.Subject.Replace("'", "") + "'," + 3 + ",'" + appt.Start.ToString("MM/dd/yyyy hh:mm tt") + "','" + appt.End.ToString("MM/dd/yyyy hh:mm tt") + "'," + IsRecurring + ",'" + appt.Location + "','" + Organizer + "','" + OrganizerItemID + "'," + isCancelled + ",'','','" + appt.Id.ChangeKey + "','" + appt.LastModifiedTime.ToString("MM/dd/yyyy hh:mm tt") + "','" + appt.ICalUid + "','" + Attendeeitemid + "','" + RoomitemID + "')"), con);
                    com.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        //ZD 100924 Start
        #region One-Way Modified DB update

        private void InsertOneWayintoDB(int isCancelled, int confid, int instanceID, int confnumname, string ICALID, DateTime startDate, DateTime EndDate, string Subject, string Location, string HostEmail)
        {
            try
            {
                SqlConnection con = new SqlConnection(connectionString);
                con.Open();
                SqlCommand com = new SqlCommand(("INSERT INTO ConferenceCreate (confID ,instanceID,ConfNumName, Subject , confStatus , StartDate , EndDate ,IsRecurring, Location , organizer ,organizerItemID, isCancelled ,inXML,outXML,changeKey,LastModified,ICALID,AttendeeitemID,LocationitemID,isExchangeConf,ErrorFetchCount) VALUES ('" + confid + "','" + instanceID + "','" + confnumname + "','" + Subject + "'," + 3 + ",'" + startDate.ToString("dd MMMM yyyy hh:mm") + "','" + EndDate.ToString("dd MMMM yyyy hh:mm") + "'," + "0" + ",'" + Location + "','" + HostEmail + "',''," + isCancelled + ",'','','','" + DateTime.UtcNow.AddMinutes(2).ToString("MM/dd/yyyy hh:mm tt") + "','" + ICALID + "','','',0,0)"), con); //ZD 103314
                com.ExecuteNonQuery();
                con.Close();
                con.Open();
                com = new SqlCommand(("INSERT INTO Conferencecreateaudit (confID ,instanceID,ConfNumName, Subject , confStatus , StartDate , EndDate ,IsRecurring, Location , organizer ,organizerItemID, isCancelled ,inXML,outXML,changeKey,LastModified,ICALID,AttendeeitemID,LocationitemID,isExchangeConf,ErrorFetchCount) VALUES ('" + confid + "','" + instanceID + "','" + confnumname + "','" + Subject + "'," + 3 + ",'" + startDate.ToString("dd MMMM yyyy hh:mm") + "','" + EndDate.ToString("dd MMMM yyyy hh:mm") + "'," + "0" + ",'" + Location + "','" + HostEmail + "',''," + isCancelled + ",'','','','" + DateTime.UtcNow.AddMinutes(2).ToString("MM/dd/yyyy hh:mm tt") + "','" + ICALID + "','','',0,0)"), con);//ZD 103314
                com.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UpdateOneWayintoDB(int isCancelled, int confid, int instanceID, int confnumname, string ICALID, DateTime startDate, DateTime End, string Subject, string Location, string HostEmail)
        {
            try
            {
                if (isCancelled == 1)
                {
                    SqlConnection con = new SqlConnection(connectionString);
                    con.Open();
                    SqlCommand com = new SqlCommand(("UPDATE ConferenceCreate Set isCancelled = 1 , LastModified = '" + DateTime.UtcNow.AddMinutes(2).ToString("MM/dd/yyyy hh:mm tt") + "' Where confID = '" + confid + "' and instanceID = '" + instanceID + "' and ICALID = '" + ICALID + "'"), con);
                    com.ExecuteNonQuery();
                    con.Close();
                    con.Open();
                    com = new SqlCommand(("INSERT INTO Conferencecreateaudit (confID ,instanceID,ConfNumName, Subject , confStatus , StartDate , EndDate ,IsRecurring, Location , organizer ,organizerItemID, isCancelled ,inXML,outXML,changeKey,LastModified,ICALID,AttendeeitemID,LocationitemID) VALUES ('" + confid + "','" + instanceID + "','" + confnumname + "','" + Subject + "'," + 3 + ",'" + startDate.ToString("dd MMMM yyyy hh:mm") + "','" + End.ToString("dd MMMM yyyy hh:mm") + "'," + "0" + ",'" + Location + "','" + HostEmail + "',''," + isCancelled + ",'','','','" + DateTime.UtcNow.AddMinutes(2).ToString("MM/dd/yyyy hh:mm tt") + "','" + ICALID + "','','')"), con);
                    com.ExecuteNonQuery();
                    con.Close();
                }
                else
                {
                    SqlConnection con = new SqlConnection(connectionString);
                    con.Open();
                    //ZD 102085 Starts
                    string sqlQry = "";
                    //ZD 103096
                    if (confid == 0)
                        sqlQry = "UPDATE ConferenceCreate Set ErrorFetchCount = 0, StartDate ='" + startDate.ToString("dd MMMM yyyy hh:mm") + "',EndDate='" + End.ToString("dd MMMM yyyy hh:mm") + "',Subject='" + Subject.Replace("'", "") + "',Location='" + Location + "',isCancelled='" + isCancelled + "',LastModified = '" + DateTime.UtcNow.AddMinutes(2).ToString("MM/dd/yyyy hh:mm tt") + "',confID = '" + confid + "', instanceID = '" + instanceID + "',  ICALID ='" + ICALID + "' Where confnumname ='" + confnumname + "'";//103314
                    else
                        sqlQry = "UPDATE ConferenceCreate Set ErrorFetchCount = 0, StartDate ='" + startDate.ToString("dd MMMM yyyy hh:mm") + "',EndDate='" + End.ToString("dd MMMM yyyy hh:mm") + "',Subject='" + Subject.Replace("'", "") + "',Location='" + Location + "',isCancelled='" + isCancelled + "',LastModified = '" + DateTime.UtcNow.AddMinutes(2).ToString("MM/dd/yyyy hh:mm tt") + "',  ICALID ='" + ICALID + "' Where confnumname ='" + confnumname + "'";//103314
                    //SqlCommand com = new SqlCommand(("UPDATE ConferenceCreate Set StartDate ='" + startDate.ToString("dd MMMM yyyy hh:mm") + "',EndDate='" + End.ToString("dd MMMM yyyy hh:mm") + "',Subject='" + Subject.Replace("'", "") + "',Location='" + Location + "',isCancelled='" + isCancelled + "',LastModified = '" + DateTime.UtcNow.AddMinutes(2).ToString("MM/dd/yyyy hh:mm tt") + "',confID = '" + confid + "', instanceID = '" + instanceID + "' Where ICALID ='" + ICALID + "'"), con); //inXML='',outXML='',
                    SqlCommand com = new SqlCommand((sqlQry), con); //inXML='',outXML='', //ZD 102085 Ends
                    com.ExecuteNonQuery();
                    con.Close();
                    con.Open();
                    com = new SqlCommand(("INSERT INTO Conferencecreateaudit (confID ,instanceID,ConfNumName, Subject , confStatus , StartDate , EndDate ,IsRecurring, Location , organizer ,organizerItemID, isCancelled ,inXML,outXML,changeKey,LastModified,ICALID,AttendeeitemID,LocationitemID) VALUES ('" + confid + "','" + instanceID + "','" + confnumname + "','" + Subject + "'," + 3 + ",'" + startDate.ToString("dd MMMM yyyy hh:mm") + "','" + End.ToString("dd MMMM yyyy hh:mm") + "'," + "0" + ",'" + Location + "','" + HostEmail + "',''," + isCancelled + ",'','','','" + DateTime.UtcNow.AddMinutes(2).ToString("MM/dd/yyyy hh:mm tt") + "','" + ICALID + "','','')"), con);
                    com.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
        //ZD 100924 End
    }
}


